from PIL import Image
from glob import glob
from pprint import pprint

d = {}
imW, imH = 512, 512

def proc_file(f):
	im = Image.open(f).convert('L')
	pix = im.load()
	#print im
	c = pix[0,0]
	for y in xrange(imH):
		for x in xrange(imW):
			c = pix[x, y]
			if c == 255:
				continue
				
			k = (x, y)
			if not k in d:
				d[k] = []

			#v = c
			v = (c, f)
			if v not in d[k]:
				d[k].append(v)
	print c

for f in glob("obj_depth*.png"):
	print f
	proc_file(f)

#print d.keys()
pprint(d[(10, 10)])
maxl = 0
for v in d.values():
	l = len(v)
	maxl = l if l > maxl else maxl
print maxl