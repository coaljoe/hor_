from PIL import Image
from glob import glob
from pprint import pprint

d = {}
imW, imH = 512, 512

def proc_file(f):
	im = Image.open(f).convert('L')
	pix = im.load()
	#print im
	c = pix[0,0]
	for y in xrange(imH):
		for x in xrange(imW):
			c = pix[x, y]
			if c == 255:
				continue
				
			k = (x, y)
			if not k in d:
				d[k] = []

			#v = c
			v = (c, f)
			if v not in d[k]:
				d[k].append(v)
			# sort
			kf = lambda x: x[0]
			d[k].sort(key=kf, reverse=True)
	print c

for f in glob("obj_depth*.png"):
	print f
	proc_file(f)

#print d.keys()
pprint(d[(10, 10)])
#pprint(d[(266, 420)])
pprint(d[(203, 55)])
maxl = 0
for v in d.values():
	l = len(v)
	maxl = l if l > maxl else maxl
print maxl

im = Image.new('RGB', (imW, imH))
pix = im.load()

im_depth = Image.new("L", im.size)
pix_depth = im_depth.load()

p = {}

for k,v in d.iteritems():
	level = 0 # -1
	if len(v) < 2:
		continue
	if len(v) < level+1:
		continue
	else:
		#print v
		#level = 0
		#level = -1
		p[k] = (v[level][0], v[level][1].replace("_depth", ""))
		
#pprint(p)

pix_cache = {}

for y in xrange(imH):
	for x in xrange(imW):
		k = (x, y)
		if k not in p:
			continue
		depth, info = p[k]
		if info not in pix_cache:
			im2 = Image.open(info)
			pix_cache[info] = im2.load()
			
		c = pix_cache[info][k]
		pix[k] = c
		
		pix_depth[k] = depth

pprint(pix_cache)
im.save("out.png")
im_depth.save("out_depth.png")