#!/bin/sh
#NVCOMPRESS=/home/j/build/nvidia-texture-tools/build/src/nvtt/tools/nvcompress
NVCOMPRESS=nvcompress

dir=$1
if [ ! -d $dir ]; then
    echo 'error: output must be a directory'
    exit -1
fi

nohup ~/util/boost.sh 20 2>&1 >/dev/null &
cd $dir

#rm -fv orig_*.png

for f in `ls -1 *.png`; do
    echo "converting $f"
    #newfn=`echo -n $f | sed -e "s:\.png:\.dds:" -e "s:\.jpg:\.dds:"`
    newfn="$f.dds"
    #newfn="$f.jpg"
    #convert -flip -define dds:compression=dxt1 -define dds:cluster-fit=true -define dds:mipmaps=false $f $newfn
    #convert -define dds:compression=dxt1 -define dds:cluster-fit=true -define dds:mipmaps=false $f $newfn
    #convert -quality 95 $f $newfn
    #convert -strip -quality 90 -sampling-factor 4:2:2 -define jpeg:dct-method=float $f $newfn

    # nvcompress
    tmpf=/tmp/tmp.tga
    #convert -quality 0 $f $tmpf
    convert -flip -quality 0 $f $tmpf
    ## fast opts.
    #-nocuda -silent -bc1 -nomips -fast $tmpf /tmp/tmp.dds
    $NVCOMPRESS -nocuda -silent -bc1 -nomips $tmpf /tmp/tmp.dds
    mv -v /tmp/tmp.dds `pwd`/$newfn
done

#sed -i -e "s:\.png:\.dds:g" -e "s:\.jpg:\.dds:g" *.dae

#rm -fv *.png *.jpg

echo "pack finished"
