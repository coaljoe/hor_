Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-08-26T16:30:17+03:00

====== Карты ======

==== Детали ====

=== Заставки ===

Стиль как в Autumn Leaf (chip disk) (autumn_leaf), 
возможно уменьшенный и отцентрованный как на тамбе на ноуте (вин7)

'''
Music.. .  .    . Filip Skutela (Lamb)                   
Programming.. .   Piotr Brendel (Pienia)                 
Graphics.. .   .  Mateusz Janik (Diodak) 
'''


=== Миникарты ===

Варианты:
1. Как в FT. [[http://2.bp.blogspot.com/-iVCMNeotbT0/UpxXGCuJbHI/AAAAAAAAlnk/UrTqlOeXiOg/s1600/Fallout_Tactics_%28PC%29_14.jpg|pic]]
2. Как в ГТА или xmoto.
3. Нормальные карты как в пипбое. [[http://fallout.wikia.com/wiki/File:Fallout_PIP-Boy_2000.jpg|pic]]
4. Никаких карт.

? На таких картах не отображается где находится персонаж.
