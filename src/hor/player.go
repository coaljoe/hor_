package main

// entity
type Player struct {
	Obj
	Character
}

func newPlayer() (t Player) {
	t = Player{
		Obj:       newObj(),
		Character: newCharacter(),
	}
	return t
}
