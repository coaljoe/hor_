package main

import (
	"bitbucket.org/coaljoe/rx"
	//. "bitbucket.org/coaljoe/rx/math"

	"github.com/go-gl/gl/v2.1/gl"
)

type NavgridView struct {
	*rx.Node
	m          *Navgrid
	d_showGrid bool
}

func newNavgridView(m *Navgrid) *NavgridView {
	v := &NavgridView{Node: rx.NewNode(rx.NodeType_Custom),
		m:          m,
		d_showGrid: true}
	// spawn
	rx.Rxi().Scene().AddRenderable(v)
	return v
}

func (v *NavgridView) destroy() {
	rx.Rxi().Scene().RemoveRenderable(v)
}

func (v *NavgridView) Render(r *rx.Renderer) {
	m := v.m
	/*
		if v.d_showGrid {
			rx.DebugDrawCube(Vec3Zero, 10, Vec3{1.0, 0.0, 0.0})
		}
	*/

	//if !vars.debugFieldGrid {
	//	// do nothing
	//	return
	//}
	return

	// prepare
	gl.Color3f(0, 1, 1)

	for i := 0; i < m.h-1; i++ {
		for j := 0; j < m.w-1; j++ {
			py := i * cell_size
			px := j * cell_size

			gl.PushMatrix()
			//gl.Translatef(float32(py), 0, float32(px))
			gl.Translatef(float32(py), float32(px), 0)

			//rx.DrawAxes()
			rx.DrawLine(0, 0, 0, 0, 0, 1)

			/*
				if fv.m.IsCellPassable(i, j, CGroundRealm) {
					gl.Translatef(float32(cell_size/2), 0, float32(cell_size/2))
					rx.DrawAxes()
				}
			*/

			gl.PopMatrix()
		}
	}
}

func (v *NavgridView) update(dt float64) {
	//r := rx.Rxi().Renderer()
	//v.render(r)
	//rx.DebugDrawCube(Vec3{0, 0, 1}, 10, Vec3{1.0, 0.0, 0.0})
}
