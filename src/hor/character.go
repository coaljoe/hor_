package main

// component
type Character struct {
	Health
	inventory Inventory
	equip     CharacterEquip
	info      CharacterInfo
}

func newCharacter() (c Character) {
	c = Character{
		Health:    newHealth(),
		inventory: newInventory(),
		equip:     newCharacterEquip(&c),
		info:      newCharacterInfo(),
	}
	//	c.equip = newCharacterEquip(&c)
	return
}

func (c *Character) pickupItem(it *Item) bool {
	if ok := c.inventory.putItem(it); ok {
		_log.Dbg("picked item; ", it)
		return true
	} else {
		_log.Dbg("can't pick item; ", it)
		return false
	}
}

func (c *Character) dropItem(it *Item) bool {
	if ok := c.inventory.takeItem(it); ok {
		_log.Dbg("dropped item; ", it)
		return true
	} else {
		_log.Dbg("can't drop item; ", it)
		return false
	}
}

// Активный предмет
func (c *Character) Item() *Item {
	return c.equip.getActiveItem()
}

/**** CharacterInfo ****/

type CharacterInfo struct {
	name            string
	age             int
	gender          string
	description     string
	longDescription string
	background      string
	portrait        string
}

func newCharacterInfo() (ci CharacterInfo) {
	ci = CharacterInfo{}
	return
}
