package main

import (
	"bitbucket.org/coaljoe/rx"
)

type BackgroundView struct {
	bg   *Background
	card *rx.Card
}

func NewBackgroundView(bg *Background) (t *BackgroundView) {
	t = &BackgroundView{
		bg:   bg,
		card: rx.NewCard(),
	}
	return
}

func (v *BackgroundView) Spawn() {
	v.card.SetTexture(v.bg.tex)
	v.card.SetZ(v.bg.z)
	v.card.Spawn()
}

func (v *BackgroundView) Destroy() {
	v.card.Destroy()
}

func (v *BackgroundView) Update(dt float64) {
}
