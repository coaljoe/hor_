package main

// from ltest

import (
	"time"
)

type Timer struct {
	id int
	speed,
	time, start_time float64
	running bool
}

var maxTimerId = 0
var globalSpeed = 1.0

func NewTimer(autostart bool) (t *Timer) {
	maxTimerId += 1
	t = &Timer{
		id:    maxTimerId,
		speed: 1.0,
	}
	t.reset()
	if autostart {
		t.Start()
	}
	timersys().AddTimer(t)
	return
}

func (t *Timer) Start() {
	t.reset()
	t.running = true
}

func (t *Timer) Restart() {
	t.Start()
}

func (t *Timer) Pause() {
	t.running = false
}

func (t *Timer) Resume() {
	t.running = true
}

func (t *Timer) reset() {
	t.time = 0
	t.start_time = now()
	//t.Start()
}

func (t *Timer) Update(dt float64) {
	if !t.running {
		return
	}
	t.time = (now() - t.start_time) * t.speed * globalSpeed
}

func (t *Timer) Dt() float64 {
	return now() - t.start_time
}

// ? first run check
func (t *Timer) Zero() bool {
	return t.time == 0
	//return false // fixme: doesn't work
}

func (t *Timer) Running() bool {
	return t.running
}

func (t *Timer) Paused() bool {
	return !t.Running()
}

func (t *Timer) TogglePause() {
	if t.running {
		t.Pause()
	} else {
		t.Resume()
	}
}

func (t *Timer) Speed() float64 { return t.speed }

func now() float64 {
	return unix_float_seconds()
}

func unix_float_seconds() float64 {
	return float64(time.Now().UnixNano()) / float64(time.Second)
}
