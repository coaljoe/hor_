package main

// from ltest

import (
	. "strconv"
)

var _timersysi *TimerSys

type TimerSys struct {
	timers map[int]*Timer
}

func NewTimerSys() *TimerSys {
	if _timersysi != nil {
		return _timersysi
	}
	t := &TimerSys{
		timers: make(map[int]*Timer),
	}
	_timersysi = t
	return _timersysi
}

func timersys() *TimerSys { return NewTimerSys() }

func (ts *TimerSys) AddTimer(t *Timer) {
	if ts.HasTimer(t) {
		panic("already have timer; id: " + Itoa(t.id))
	}
	ts.timers[t.id] = t
}

func (ts *TimerSys) RemoveTimer(t *Timer) {
	delete(ts.timers, t.id)
}

func (ts *TimerSys) HasTimer(t *Timer) bool {
	return ts.timers[t.id] != nil
}

func (ts *TimerSys) Update(dt float64) {
	for _, t := range ts.timers {
		t.Update(dt)
	}
}
