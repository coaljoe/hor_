// hor
package main

import (
	"fmt"
	"math/rand"
	"os"

	"bitbucket.org/coaljoe/lib/xlog"
	//_path "path"
	"path/filepath"
	//"reflect"
	"time"

	. "bitbucket.org/coaljoe/rx/math"

	"bitbucket.org/coaljoe/rx"

	"github.com/davecgh/go-spew/spew"
	"github.com/go-gl/gl/v2.1/gl"
	"github.com/go-gl/glfw/v3.1/glfw"
)

//var _ = reflect.Bool
var _ = Vec3Zero
var _ = spew.Dump

/*
// package init
func init() {
	println("....init")
}
*/

// entry point
func main() {
	fmt.Println("main()")
	/*
		// doesn't work with .exe linking
		if runtime.GOOS == "windows" {
			envPath := os.Getenv("PATH")
			nEnvPath := envPath + string(os.PathListSeparator) + _path.Join("lib", "dll")
			os.Setenv("PATH", nEnvPath)
			fmt.Println(os.Environ())
			fmt.Println(nEnvPath, " ", os.Getenv("PATH"))
			panic(2)
		}
	*/
	// cd to app's root
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		panic(err)
	}
	fmt.Println("dir:", dir)
	appRoot := filepath.Join(dir, "..")
	fmt.Println("appRoot: ", appRoot)
	os.Chdir(appRoot)
	//panic(2)

	Main()
}

func Main() {
	println("hor.Main()")
	Init()

	// set config
	rx.SetDefaultConf()
	rx.ConfSetResPath("res")
	//time.Sleep(1 * time.Second)

	//rx.Init()
	/*
		win := rx.NewWindow()
		//win.Init(640, 480)
		//win.Init(1024, 576)
		win.Init(960, 540) // 1920/2, 1080/2
		app := rx.NewApp(win)
		app.Init()
	*/
	rxi := rx.Init(960, 540)
	app := rxi.App
	win := app.Win()
	//time.Sleep(1 * time.Second)
	//rxi := rx.Rxi()
	//rxi.SetResPath("res/")
	//rxi.SetResPath("/home/j/dev/misc/ltest/go/src/rx/res/")
	_ = rxi

	// init game

	initNewGame(app)

	//game := NewGame(app)
	//time.Sleep(1 * time.Second)

	// globals

	var bg *TiledBackground
	var g3 *rx.Node
	var box *rx.Node
	_ = box

	/**** Scene 1 ****/

	scene1 := func() {

		// setup camera
		c := _Scene.camera
		c.cam.Camera.SetZnear(1)
		c.cam.Camera.SetZfar(100)
		//c.cam.SetZfar(100)
		c.cam.Camera.SetFov(10)
		//c.pos = Vec3{5, 5, 5}
		c.setPos(Vec3{10, 5, 10})
		//c.pos = Vec3{15, 10, 15}
		//c.pos = Vec3{30, 30, 30}

		// new rot-based coords
		c.setPos(Vec3{10, -10, 5})
		c.setRot(Vec3{70.562, 0, 45})

		c.zoom = 1
		c.defaultZoom = 1
		p(c.cam.Camera.Zoom())

		// check depth range
		var dr [2]float32
		gl.GetFloatv(gl.DEPTH_RANGE, &dr[0])
		fmt.Println("DepthRange: ", dr)
		if !(dr[0] == 0 && dr[1] == 1) {
			panic("bad depth range")
		}

		// render target

		/*
			w, h := app.Win().Size()
			//rxi.Camera.AddRenderTarget("camRT", w, h, true)
			c.cam.AddRenderTarget("camRT", w, h, true)
		*/

		// set up renderer

		/*
			rx.Xglcheck()
			r := rxi.Renderer()
			rp := rx.NewTestRenderPass()
			rp.Init(r)
			r.AddRenderPass(rp)
			//r.SetDefaultRenderPass(false)
			rx.Xglcheck()
		*/

		/*
			rx.Xglcheck()
			r := rxi.Renderer()
			rp := rx.NewDepthRenderPass()
			rp.Init(r)
			r.AddRenderPass(rp)
			r.SetDefaultRenderPass(false)
			rx.Xglcheck()
		*/

		// var _ = `
		rx.Xglcheck()
		r := rxi.Renderer()
		rp := rx.NewDepthCombRenderPass()

		bgTex := rx.NewTextureOpt("res/test/bg/bg.png", rx.TextureOptions{Flip: true})
		rp.BgTex = bgTex

		bgDepthTex := rx.NewTextureOpt("res/test/bg/bg_depth.png", rx.TextureOptions{Flip: true})
		rp.BgDepthTex = bgDepthTex

		rp.Init(r)
		r.AddRenderPass(rp)

		//rp2 := rx.NewTestRenderPass()
		//rp2.Init(r)
		//r.AddRenderPass(rp2)

		r.SetDefaultRenderPass(false)
		rx.Xglcheck()
		//`

		/*
			r := rxi.Renderer()
			rp := rx.NewTestRenderPass()

			rp.Init(r)
			r.AddRenderPass(rp)
			r.SetDefaultRenderPass(false)
			rx.Xglcheck()
		*/

		// game block

		sl := rx.NewSceneLoader()
		_ = sl

		//sl.Load("res/test/bg/scene/model.dae")
		//sl.Load("res/test/bg/scene2/scene.dae")
		//sl.Load("res/test/bg/scene3/scene.dae")
		nodes := sl.Load("res/test/bg/kid/model.dae")
		sl.Spawn()

		nodes[0].SetPos(Vec3{2, 0.1, 0})
		g3 = nodes[0]

		/*
			sl.Load("res/units/ground/heavytank/reds/model/model.dae")
			sl.Spawn()
		*/

		/*
			sl.Load("/home/j/dev/go/src/rx/res/test/sphere_array/spherearray.dae")
			sl.Spawn()
		*/

		/*
			bg := NewBackground()
			bg.Load("res/test/bg/bg.png")
			bg.SetZ(-1)
			bg.Spawn()
		*/

	}
	var _ = scene1

	/**** Scene 2 ****/

	scene2 := func() {

		// setup camera
		c := _Scene.camera
		c.cam.Camera.SetZnear(1)
		c.cam.Camera.SetZfar(100)
		//c.cam.SetZfar(100)
		//c.cam.SetFov(10)
		c.cam.Camera.SetFov(25)
		//c.pos = Vec3{5, 5, 5}
		//c.pos = Vec3{3.33, 7.68, 7.49}
		//c.pos = Vec3{3.33, 7.68, -7.49}
		//c.pos = Vec3{3, 8, -8}
		c.setPos(Vec3{3, 8, 8})
		//c.pos = Vec3{3, 3, 3}
		//c.cam.SetTarget(Vec3{29.21808, 85.69572, -17.74107})
		//t := Vec3{29.21808, -17.74107, 85.69572}
		//t := Vec3{5.91818, 5.13789, 1.82858} // z+10
		//t := Vec3{5.58880, 5.50064, 1.33013} // z+10
		t := Vec3{5.58880, 5.50064, -1.33013} // z+10
		// test
		//c.pos = Vec3{10, 10, 10}
		/*
			c.pos = Vec3{0, .1, 0}
			t = Vec3{0, 0, 1}
		*/
		/*
			c.pos = Vec3{10, .1, 10}
			c.pos = Vec3{10, -10.1, 10}
		*/
		//t = Vec3{-1, 0, -1}
		/*
			c.pos = Vec3{1, 10, 1}
			t = Vec3{0, -10, 0}
		*/
		//t = Vec3{0, 0, 0}
		//t = c.pos

		c.setPos(Vec3{3, -8, 8})
		//c.pos = Vec3{1, -6, 6}
		t = Vec3{5.58880, 1.33013, 5.50064} // z+10
		//t = Vec3{5.58880, 1.33013, 5.50064} // z+10
		//t = Vec3{28.88804, 85.30128, -16.99365}
		t = Vec3{5.58725, 1.30675, 5.41339} // z+10
		//t = Vec3{5.50000, 1.13013, 5.41181} // z+10

		//c.pos = Vec3{-10, -10, 10}
		//t = Vec3{0, 0, 0}

		// fixme
		c.setPos(Vec3{3, -8, 8})
		c.setRot(Vec3{75, -2, -15})

		var _ = t
		//c.trg = t
		//c.cam.SetTarget(t)
		c.update(0)
		//p("trg=",c.trg)
		//p("target=",c.cam.Target())
		p("pos=", c.cam.Pos())
		//pp(c.trg)
		//c.cam.SetRot(Vec3{14.736, 15.0, 2.0})
		//c.pos = Vec3{15, 10, 15}
		//c.pos = Vec3{30, 30, 30}
		c.zoom = 1
		c.defaultZoom = 1
		p(c.cam.Camera.Zoom())

		rx.Xglcheck()
		r := rxi.Renderer()
		rp := rx.NewDepthCombRenderPass()

		bgTex := rx.NewTextureOpt("res/test/levbg/bg.png", rx.TextureOptions{Flip: true})
		rp.BgTex = bgTex

		bgDepthTex := rx.NewTextureOpt("res/test/levbg/bg_depth.png", rx.TextureOptions{Flip: true})
		rp.BgDepthTex = bgDepthTex

		rp.Init(r)
		r.AddRenderPass(rp)
		r.SetDefaultRenderPass(false)
		rx.Xglcheck()

		// game block

		sl := rx.NewSceneLoader()
		_ = sl

		//sl.Load("res/test/bg/scene/model.dae")
		//sl.Load("res/test/bg/scene2/scene.dae")
		//sl.Load("res/test/bg/scene3/scene.dae")
		nodes := sl.Load("res/test/bg/kid/model.dae")
		sl.Spawn()

		//nodes[0].SetPos(Vec3{2, 0.1, 0})
		//nodes[0].SetPos(Vec3{1, 0, 10})
		nodes[0].SetPos(Vec3{5, 10, 0})

		nodes1 := sl.Load("res/test/bg/kid/model.dae")
		sl.Spawn()
		//nodes1[0].SetPos(Vec3{1, 1, 0})
		nodes1[0].SetPos(Vec3{9, 3, 0})

		nodes2 := sl.Load("res/test/bg/kid/model.dae")
		sl.Spawn()
		//nodes1[0].SetPos(Vec3{1, 0, 1})
		nodes2[0].SetPos(Vec3{17, 0, 0})

		g3 = nodes[0]
	}
	var _ = scene2

	/**** Scene 3 ****/

	scene3 := func() {

		// setup camera
		c := _Scene.camera
		c.cam.Camera.SetZnear(1)
		c.cam.Camera.SetZfar(100)
		//c.cam.SetZfar(100)
		//c.cam.SetFov(10)
		c.cam.Camera.SetFov(18.75) // proportional scale to resolution (960*(10/512))
		//c.pos = Vec3{5, 5, 5}
		c.setPos(Vec3{10, 5, 10})
		//c.pos = Vec3{15, 10, 15}
		//c.pos = Vec3{30, 30, 30}

		// new rot-based coords
		//c.pos = Vec3{10, -10, 5}
		//c.rot = Vec3{70.562, 0, 45}

		// fixme
		//c.pos = Vec3{3, -8, 8}
		//c.pos = Vec3{-12.93624, -46.53374, 12.93307} // xyz rot
		//c.pos = Vec3{-14.17790, -46.17058, 12.93307} // yxz rot
		//c.pos = Vec3{-14.177902221679688, -46.17058181762695, 12.933069229125977} //yxz rot, precise
		//c.rot = Vec3{75, -2, -15}
		c.setPos(Vec3{-12.936243057250977, -46.53374099731445, 12.933069229125977}) // yxz rot, converted precise
		//c.setRot(Vec3{74.87029337963202, -7.684147231371935, -7.579197105906458})   // yxz, converted precise (from xyz 75, -2, -15)
		c.setRot(Vec3{74.99997818140304, -2.0000023323847937, -14.999998709865736}) // xyz, precise
		//c.rot = Vec3{75.0, -2.0, -15.0} // xyz, not presice

		//c.pos = Vec3{-12.936243057250977, -46.53374099731445, 12.936243057250977}
		//c.rot = Vec3{74.99999867197056, -2.000000197950678, -15.000000417413029}

		/*
			>>> d.objects["Camera.007"].matrix_world.to_translation()
			Vector((-12.936243057250977, -46.53374099731445, 12.933069229125977))

			>>> [math.degrees(x) for x in d.objects["Camera.007"].rotation_euler]
			[74.99997818140304, -2.0000023323847937, -14.999998709865736]
		*/

		c.zoom = 1
		c.defaultZoom = 1
		p(c.cam.Camera.Zoom())

		// fixme? c.cam.Pos() = 30, 30, 30
		c.update(0)

		/*
			p(c.cam.Pos())
			//c.cam.MoveByVec(Vec3{0,100,0})
			c.cam.MoveByVec(Vec3{0,0,-50})
			//c.Update(0)
			p(c.cam.Rot())
			pp(c.cam.Pos())
		*/

		/*
			p(c.cam.Pos())
			c.setPos(Vec3Zero)
			c.build()
			c.Update(0)
			//c.cam.SetPos(Vec3Zero)
			p(c.cam.Pos())
			//c.cam.MoveByVec(Vec3{0,100,0})
			c.cam.MoveByVec(Vec3{0,0,50})
			//c.Update(0)
			p(c.cam.Rot())
			pp(c.cam.Pos())
		*/

		// check depth range
		var dr [2]float32
		gl.GetFloatv(gl.DEPTH_RANGE, &dr[0])
		fmt.Println("DepthRange: ", dr)
		if !(dr[0] == 0 && dr[1] == 1) {
			panic("bad depth range")
		}

		/*
			p(c.cam.Pos())
			c.cam.MoveByVec(Vec3{10.0, 10.0, 0})
			p(c.cam.Pos())
			pp(2)
		*/

		// render target

		/*
			w, h := app.Win().Size()
			//rxi.Camera.AddRenderTarget("camRT", w, h, true)
			c.cam.AddRenderTarget("camRT", w, h, true)
		*/

		// set up renderer

		// game block

		sl := rx.NewSceneLoader()
		_ = sl

		//sl.Load("res/test/bg/scene/model.dae")
		//sl.Load("res/test/bg/scene2/scene.dae")
		//sl.Load("res/test/bg/scene3/scene.dae")
		nodes := sl.Load("res/test/bg/kid/model.dae")
		sl.Spawn()
		_ = nodes

		//nodes[0].SetPos(Vec3{2, 0, 0.1})
		//nodes[0].MoveByVec(Vec3{-2, 0, 0})
		nodes[0].SetPos(Vec3{4, 8, .5})

		g2 := nodes[0].Clone()
		g2.SetPos(Vec3{12, -3, 0})
		g2.SetRot(Vec3{0.0, 0, -45.0})
		//g2.Mesh.Spawn()
		rxi.Scene().Add(g2)

		g3 = nodes[0].Clone()
		g3.SetPos(Vec3{0.0, 16, .5}) // above ground
		g3.SetRot(Vec3{0.0, 0, -90.0})
		//g3.Mesh.Spawn()
		rxi.Scene().Add(g3)

		g4 := nodes[0].Clone()
		g4.SetPos(Vec3{3, 25, 10})
		g4.SetRot(Vec3{0, 0, 0})
		//g4.Mesh.Spawn()
		rxi.Scene().Add(g4)

		/*
			sl.Load("res/units/ground/heavytank/reds/model/model.dae")
			sl.Spawn()
		*/

		/*
			sl.Load("/home/j/dev/go/src/rx/res/test/sphere_array/spherearray.dae")
			sl.Spawn()
		*/

		//ns := sl.Load("/home/j/dev/go/src/rx/res/models/primitives/box/box.dae")
		ns := sl.Load("tmp/rooms_dev/cutaway_obj.dae")
		sl.Spawn()
		box = ns[0]
		box.SetVisible(false)

		// tuning
		rx.ConfSetDebugDraw(true)

		//bg = newTiledBackground(10, 5, win.Width(), win.Height())
		//bg = newTiledBackground(4, 4, win.Width(), win.Height(), c)
		bg = newTiledBackground(6, 2, win.Width(), win.Height(), c)
		//bg = newTiledBackground(1, 1, win.Width(), win.Height(), c)
		//bg.load("res/test/bg/bg.png")
		//bg.load("/home/j/tmp/")
		//bg.load("tmp/")
		//bg.load("tmp/mall_exterior/")
		//bg.load("tmp/mall_exterior_noaa/")
		//bg.load("tmp/mall_exterior_daytrans/t00/") // temporary? fixme?
		bg.load("tmp/render2/t00/")
		bg.shx = 0 //-1000
		bg.shy = 0 //-1000
		bg.spawn()
		//bg.visible = false // FIXME: doesn't work

		// daytrans
		d := _Daytrans
		// monkey patch (disables daytrans)
		d.maxn = 1
		d.active = false
		//d.init(bg, "tmp/mall_exterior_daytrans/")
		d.init(bg, "tmp/render2/")

		// renderpass

		rx.Xglcheck()
		r := rxi.Renderer()
		r.SetDefaultRenderPass(false)

		rp := NewTDepthCombRenderPass(bg)

		//bgTex := rx.NewTexture("res/test/levbg/bg.png")
		//rp.BgTex = bgTex

		//bgDepthTex := rx.NewTexture("res/test/levbg/bg_depth.png")
		//rp.BgDepthTex = bgDepthTex

		//dtex := rx.NewTexture("tmp/kid_depth_000_000.png")
		//rp.dtex = dtex

		rp.Init(r)
		rp.SetCharacterObj(g3)
		r.AddRenderPass(rp)

		// rp2

		rp2 := rx.NewFxaaRenderPass()
		rp2.SetPrevRenderPass(rp.RenderPass)
		rp2.Init(r)
		r.AddRenderPass(rp2)
		rx.Xglcheck()

		// test cam
		//c.Translate(1, 0, 0)
		//c.Translate(1, 0, 0)
		//c.Translate(10, 0, 0)
		//pw := 1.0 / float64(bg.vpw)
		//ph := 1.0 / float64(tb.vph)
		//uSizePx := 512.0/10.0
		pxSizeU := 10.0 / 512.0
		var _ = pxSizeU
		//c.Pan(1 * pxSizeU, 0)
		//c.Pan(111 * pxSizeU, 0)
		//c.Pan(112 * pxSizeU, 0)
		//c.Pan(113 * pxSizeU, 0)
		//c.Pan(114 * pxSizeU, 0)
		//c.Pan(9 * pxSizeU, 11 * pxSizeU)
		//bg.pan(100, 0)
		//bg = nil

		// Navgrid
		ng := newNavgrid(10, 10)
		p("spawn...")
		ng.spawn()
		p("done")
		_ = ng

	}
	var _ = scene3

	//scene1()
	//scene2()
	scene3()
	//kefTest()
	//pp(2)

	// update block //

	//fps_limit := 0

	for app.Step() {
		//println("main.step")
		//rx.DebugDrawCube(Vec3Zero, 5, Vec3{1.0, .5, .5})
		dt := app.GetDt()
		game.update(dt)
		//rx.DebugDrawCube(Vec3Zero, 5, Vec3{1.0, .5, .5})

		/*
			if fps_limit != 0 {
				//ms_to_sleep := int64((1e6 / int64(fps_limit)) - app.GetDtMs())
				//time.Sleep(time.Duration(ms_to_sleep) * time.Millisecond)
				s_to_sleep := (1.0 / float64(fps_limit)) - dt
				p(s_to_sleep)
				if s_to_sleep > 0.0 {
					sleepsec(s_to_sleep / 2)
				}
				//sleepsec(0.5)
			}
		*/

		if g3 != nil {

			px := 0.
			py := 0.
			speed := 10.0 * dt

			kb := _InputSys.Keyboard
			if kb.IsKeyPressed(glfw.KeyW) {
				py = speed
			} else if kb.IsKeyPressed(glfw.KeyS) {
				py = -speed
			}
			if kb.IsKeyPressed(glfw.KeyA) {
				px = -speed
			} else if kb.IsKeyPressed(glfw.KeyD) {
				px = speed
			}

			g3.Translate(Vec3{px, py, 0})
			//g3.SetVisible(false)
			//g2.Translate(Vec3{px, py, 0})
			if box != nil {
				box.SetPos(g3.Pos())
				box.SetRot(Vec3{75, -2, -15}) // same as camera
				// move box closer to camera from origin
				box.MoveByVec(Vec3{0, 0, 1.0})
				//pp(box.Rot())
				//box.SetPos(g3.Pos().Add(Vec3{-0.5, -2.0, 1}))
				//pp(box.Id())
			}

			/*
				// project
				ob := g3
				//obj := ob.Pos() // object coords
				obHeight := 1.8
				//obWidth := 0.4
				obj := ob.Pos().Add(Vec3{0, 0, obHeight / 2.0})
				cam := _Scene.camera.cam
				//modelviewM := ob.Mat().Mul(cam.GetViewMatrix())
				modelviewM := cam.GetViewMatrix()
				projM := cam.GetProjectionMatrix()
				// opengl window coords
				win := Project(obj, modelviewM, projM, 0, 0, rxi.Renderer().Width(), rxi.Renderer().Height())
				fmt.Println("win: ", win)
			*/
		}

		//sleepsec(0.1)
		//pp(u2.GetTargets())

		// update tiled bg only if presented
		if bg != nil {
			//bg.Update(dt)
			bg.update(dt)
		}
	}

	println("exiting...")
}

func kefTest() {
	println("kef test")

	// Player test

	pl := newPlayer()
	pl.showHealth()
	pl.setHealth(10)
	pl.showHealth()
	pl.takeHealth(10)
	if !pl.isAlive() {
		println("killed", fmt.Sprintf("(id %d)", pl.id))
	}

	// Item test

	it := newItem()
	it.name = "Test item"

	ic := newItemContainer()
	ic.addItem(it)
	fmt.Println(ic.items)
	p(it.isAppliable())

	apl := newAppliableItem(it)
	it.appliable = &apl
	p(it.isAppliable())
	it.appliable.apply()

	ei := newEquipableItem(it)
	it.equipable = &ei
	p(it.isEquipable())
	p(it.equipable.insulation)

	// Item factory test

	it2 := makeItem("bottle")
	p(it2)
	p(&it2)
	//Printf("%T %+v\n", it2, it2)
	//spew.Dump(it2.appliable)
	//spew.Printf("%v", it2)
	if it2.isAppliable() {
		it2.appliable.apply()
	} else {
		panic("appliable check failed")
	}

	// Item interaction test

	pl.pickupItem(it2)
	pl.dropItem(it2)

	// Equip test

	it3 := makeItem("boots")
	pl.equip.putItem(it3)
	//pl.equip.putItem(it2)
	pl.equip.showEquip()

	// Inventory test

	pl.inventory.putItem(it2)
	pl.inventory.cont.showItems()
	pl.equip.putItem(it2)
	pl.inventory.cont.showItems()
	pl.equip.showEquip()
	p("Player's active item:", pl.Item())

	// Weapon test
	w := makeItem("g17")
	wc := w.weapon
	wc.reload()
	wc.fire()
	wc.fire()

	// World test
	wd := _World
	//wd.setDefaultWorld()
	p("worlds equal", *_World == game.state.world)
	p("worlds equal", _World == &game.state.world)
	//p("worlds equal", _World == game.state.world)
	p(wd.timeOfDay())
	p(wd.daytime)
	p(wd.daytimeStr())
	p(wd.test)
	wd.test = false
	p(wd.test)

	println("kef exiting...")
}

/**** Init ****/

var (
	_log *xlog.Logger
)

func Init() {
	println("hor Init() begin")

	rand.Seed(time.Now().UTC().UnixNano())

	// create logger
	_log = xlog.NewLogger("hor", xlog.LogDebug)
	if loglev := os.Getenv("LOGLEVEL"); loglev != "" {
		_log.SetLogLevelStr(loglev)
	}
	println("hor Init() done")
}
