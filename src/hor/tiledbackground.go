package main

import (
	"fmt"
	. "math"
	"os"
	"bitbucket.org/coaljoe/rx"
	. "bitbucket.org/coaljoe/rx/math"
	//"github.com/go-gl/glfw/v3.1/glfw"
)

var _ = Cos

type Layer struct {
	name    string
	texs    [][]*rx.Texture // layer textures
	w, h    int
	visible bool
}

func newLayer(name string, w, h int) *Layer {
	l := &Layer{
		name:    name,
		w:       w,
		h:       h,
		texs:    make([][]*rx.Texture, 0),
		visible: true,
	}

	l.texs = make([][]*rx.Texture, l.w)
	for i := range l.texs {
		l.texs[i] = make([]*rx.Texture, l.h)
	}
	return l
}

func (l *Layer) load(path, prefix string, unpackDepth bool) {
	_log.Inf("Loading layer '" + l.name + "'...")
	// load X'Y tiles
	for y := 0; y < l.h; y++ {
		for x := 0; x < l.w; x++ {
			//ftype := ".jpg"
			ftype := ".dds"
			ftypeFound := false
			notFound := false
			loadSuccess := false

			// load layer texs
			fp := path + fmt.Sprintf("%s_%02d'%02d.png", prefix, x, y)
			if _, err := os.Stat(fp + ftype); err == nil {
				fp += ftype
				ftypeFound = true
			} else {
				if _, err := os.Stat(fp); err != nil {
					_log.Err("[Layer] layer texture not found", fp, l.name, path, prefix)
					notFound = true
					//pp("layer texture not found", fp, l.name, path, prefix, ftype)
				}
			}

			if !notFound {
				print("loading ", fp)
				if !ftypeFound {
					print(" (" + ftype + " not found)")
				}
				print("\n")
			}

			var tex *rx.Texture
			if notFound {
				// reture empty texture (fixme: reuse a single texture)
				tex = rx.NewEmptyTexture(512, 512, rx.TextureFormatRGBA)
				rx.Xglcheck()
			} else if !unpackDepth {
				tex = rx.NewTextureOpt(fp, rx.TextureOptions{Nomipmaps: true, Flip: true})
				rx.Xglcheck()
				loadSuccess = true
			} else {
				tex = rx.NewTextureOpt(fp, rx.TextureOptions{Nomipmaps: true, Flip: true})
				rx.Xglcheck()
				loadSuccess = true

				/*
					in_tex := rx.NewTextureOpt(fp, rx.TextureOptions{Nomipmaps: true, Flip: true})
					rx.Xglcheck()
					imData := in_tex.GetImageData()
					rx.Xglcheck()
					ih, iw, d := imData.H, imData.W, imData.Data
					var newD []float32
					newD = make([]float32, iw*ih*4)
					j := 0
					for y := 0; y < ih; y++ {
						for x := 0; x < iw; x += 4 {
							rv := int(d[y*iw+x])
							gv := int(d[y*iw+(x+1)])
							//println(rv, gv)
							//f := (rv + (gv >> 8)) * 0xffff / 1000.0
							//f := ((rv >> 8) + g) / 0xffff
							newD[j] = float32(f)
							j += 1
						}
					}
					tex = rx.NewEmptyTexture(iw, ih, rx.TextureFormatFloat32)
				*/
				/*
					var newD []float32
					newD = make([]float32, 512*512*4)
					tex = rx.NewEmptyTexture(512, 512, rx.TextureFormatFloat32)
					rx.Xglcheck()
					tex.SetTextureData(newD)
					rx.Xglcheck()
					//panic(2)
				*/
				rx.Xglcheck()
			}
			l.texs[x][y] = tex

			if loadSuccess {
				_log.Inf("[Layer] loaded layer texture: ", fp)
			}
		}
	}
}

//const tileSize = 512

type TiledBackground struct {
	texs          [][]*rx.Texture // active textures
	layers        map[string]*Layer
	w, h          int // total size in tiles
	shx, shy      int //
	vpw, vph      int // viewport size in pixels
	z             float64
	tileSize      int
	activeLayer   *Layer
	visible       bool
	cam           *Camera
	camInitialPos Vec3
	view          *TiledBackgroundView
}

func newTiledBackground(w, h int, vpw, vph int, cam *Camera) *TiledBackground {
	tb := &TiledBackground{
		w: w, h: h,
		vph: vph, vpw: vpw,
		tileSize: 512,
		visible:  true,
		cam:      cam,
		layers:   make(map[string]*Layer, 0),
	}

	//tb.addLayer(newLayer("zero", w, h)) // active layer, or alias. doesn't have any pre-loaded textures.
	tb.addLayer(newLayer("bg", w, h))
	tb.addLayer(newLayer("bg_depth", w, h))
	//tb.addLayer(newLayer("wall", w, h))
	//tb.addLayer(newLayer("wall_depth", w, h))
	//tb.addLayer(newLayer("wall_overlap0", w, h))
	//tb.addLayer(newLayer("wall_overlap0_depth", w, h))
	//tb.layers["wall"].visible = false // hide layer

	/*
		for n := 0; n < tb.daytransN; n++ {
			nXX := fmt.Sprintf("%02d", n)
			tb.addLayer(newLayer("bg_t"+nXX, w, h))
			if tb.hasLayer("wall") {
				tb.addLayer(newLayer("wall_t"+nXX, w, h))
			}
		}
	*/

	// save cam initial pos
	tb.camInitialPos = cam.pos()

	// set default layer
	tb.setLayer("bg")
	return tb
}

func (tb *TiledBackground) addLayer(l *Layer) {
	if tb.hasLayer(l.name) {
		pp("already have this layer", l.name)
	}
	tb.layers[l.name] = l
}

func (tb *TiledBackground) hasLayer(name string) bool {
	return tb.layers[name] != nil
}

func (tb *TiledBackground) load(path string) {
	tb.layers["bg"].load(path, "bg", false)
	tb.layers["bg_depth"].load(path, "bg_depth", true)
	//tb.layers["wall"].load(path, "wall", false)
	//tb.layers["wall_depth"].load(path, "wall_depth", true)
	//tb.layers["wall_overlap0"].load(path, "wall_overlap0", false)
	//tb.layers["wall_overlap0_depth"].load(path, "wall_overlap0_depth", true)
}

func (tb *TiledBackground) spawn() {
	tb.view = newTiledBackgroundView(tb)
	tb.view.Spawn()
}

func (tb *TiledBackground) setLayer(name string) {
	if !tb.hasLayer(name) {
		for k, v := range tb.layers {
			println(k, v)
		}
		pp("layer not found; name: " + name)
	}
	tex := tb.layers[name].texs
	if tex == nil {
		pp("unknown layer name:", name)
	}
	tb.texs = tex
}

func (tb *TiledBackground) setLayerIfVisible(name string) bool {
	if !tb.layers[name].visible {
		return false
	}

	tb.setLayer(name)
	return true
}

func (tb *TiledBackground) setVisible(v bool) {
	tb.visible = v
}

// Return vp size in tiles.
func (tb *TiledBackground) vpSizeTiles() (int, int) {
	// rendering size in tiles
	sx := int(tb.vpw / tb.tileSize)
	sy := int(tb.vph / tb.tileSize)

	return sx, sy
}

/*
func (tb *TiledBackground) pan(x, y float64) {
	pw := 1.0 / float64(tb.vpw)
	ph := 1.0 / float64(tb.vph)
	tb.shx = int(x * pw)
	tb.shy = int(y * ph)
}
*/
func (tb *TiledBackground) update(dt float64) {
	// camera unit size in pixels
	uSizePx := 512.0 / 10.0

	px := 0
	py := 0

	var _ = `

	speed := int(1000.0 * dt)
	px := 0
	py := 0

	kb := _InputSys.Keyboard
	if kb.IsKeyPressed(glfw.KeyKP8) {
		py = -speed
	} else if kb.IsKeyPressed(glfw.KeyKP2) {
		py = speed
	}
	if kb.IsKeyPressed(glfw.KeyKP4) {
		px = speed
	} else if kb.IsKeyPressed(glfw.KeyKP6) {
		px = -speed
	}
	//p(px, py)


	posDiff := tb.camInitialPos.Sub(tb.cam.pos())
	//posDiff := tb.cam.pos.Sub(tb.camInitPos)
	ary := Radians(tb.cam.rot().Z())
	//ary := Radians(tb.cam.rot.Y())
	var _ = ary
	var _ = posDiff
	var _ = uSizePx
	/*
		px = int(posDiff.X * uSizePx)
		py = int(posDiff.Y() * uSizePx)
	*/
	//px = int(-Sin(ary) * posDiff.X * uSizePx)
	//py = int(-Cos(ary) * posDiff.Y() * uSizePx)
	//px = int(-Sin(ary) * posDiff.X * uSizePx * (vars.scrollSpeed * dt))
	//py = int(-Cos(ary) * posDiff.Y() * uSizePx * (vars.scrollSpeed * dt))
	/*
		>>> v = ob2.location.z - ob1.location.z
		>>> v/10
		0.9659257888793945
	*/
	//k := 1+ (1-0.96593)
	k := 1 + (1 - 0.9659257888793945)
	kx := 1 + (1 - 0.956612491607666)
	var _ = k
	var _ = kx
	//k = 1.0
	px = 0
	//px = int(-Sin(ary) * posDiff.X * uSizePx * k)
	//px = int(posDiff.X * uSizePx)
	//py = -int(-Cos(ary) * posDiff.Z * uSizePx)
	//py = -int(-Cos(ary) * posDiff.Z * uSizePx * k)
	//py = int(posDiff.Z * uSizePx)
	//py = int(posDiff.Z * uSizePx * k)
	//px = int(posDiff.X * kx * uSizePx)
	//p(px)
	//py = int(-Cos(ary))
	`

	py = -int(tb.cam._moveAdj.Y() * uSizePx)
	px = -int(tb.cam._moveAdj.X() * uSizePx)
	//p(tb.cam._moveAdj.X)
	//pp(px)
	//px = -52
	//px = -100
	//py = -10

	if px != 0 || py != 0 {
		tb.shx = px
		tb.shy = py
	}
	/*
		// update view
		if tb.view != nil {
			tb.view.update(dt)
		}
	*/
}
