package main

// component
type CharacterEquip struct {
	cont ItemContainer
	char *Character
	head,
	body,
	legs,
	foots,
	hands,
	back ItemSlot
	// universal item slots
	item1,
	item2 ItemSlot

	activeItem *Item
}

func newCharacterEquip(ch *Character) (t CharacterEquip) {
	t = CharacterEquip{
		cont:  newItemContainer(),
		char:  ch,
		head:  newItemSlot("head", "Head"),
		body:  newItemSlot("body", "Body"),
		legs:  newItemSlot("legs", "Legs"),
		foots: newItemSlot("foots", "Foots"),
		hands: newItemSlot("hands", "Hands"),
		back:  newItemSlot("back", "Back"),
		item1: newItemSlot("item1", "Item 1"),
		item2: newItemSlot("item2", "Item 2"),
	}
	return
}

func (ce *CharacterEquip) canWear(it *Item) bool {
	if it.isEquipable() {
		return true
	}
	return false
}

func (ce *CharacterEquip) wear(it *Item) bool {
	if !ce.canWear(it) {
		_log.Dbg("can't wear item;", it)
		return false
	}
	it.equipable.wearer = ce.char
	return false
}

// Положить итем в слот.
func (ce *CharacterEquip) putItemInSlot(it *Item, itemSlotId string) {
	if itemSlotId == "" {
		// взать в слоты предметов
		done := false
		for _, slot := range ce.itemslots() {
			if slot.isEmpty() {
				if slot.item == it {
					_log.Dbg("item already in this slot, cant put here")
					continue
				}
				slot.setItem(it)
				done = true
				break
			}
		}
		if !done {
			_log.Err("can't put item;", it, itemSlotId)
		} else {
			_log.Dbg("item put in items slots")
			ce.activeItem = it
		}
	} else {
		// положить в соотв. слот
		slot := ce.getSlotById(itemSlotId)
		slot.setItem(it)

		// одеть как equipable
		if ce.canWear(it) {
			ce.wear(it)
		}

		_log.Dbg("item put in a equip slot")
	}
	it.cont.swapItem(it, &ce.cont)
}

// Coкр. для putItemInSlot.
func (ce *CharacterEquip) putItem(it *Item) {
	if it.isEquipable() {
		ce.putItemInSlot(it, it.equipable.itemSlotId)
	} else {
		ce.putItemInSlot(it, "")
	}
}

// Получить активный предмет.
func (ce *CharacterEquip) getActiveItem() *Item {
	return ce.activeItem
}

func (ce *CharacterEquip) slots() []*ItemSlot {
	return []*ItemSlot{&ce.head, &ce.body, &ce.legs,
		&ce.foots, &ce.hands, &ce.back, &ce.item1, &ce.item2}
}

func (ce *CharacterEquip) itemslots() []*ItemSlot {
	return []*ItemSlot{&ce.item1, &ce.item2}
}

func (ce *CharacterEquip) getSlotById(id string) *ItemSlot {
	found := false
	for _, slot := range ce.slots() {
		if slot.id == id {
			found = true
			return slot
		}
	}
	if !found {
		pp("no such slot id", id)
	}
	return nil
}

func (ce *CharacterEquip) showEquip() {
	println("#CharacterEquip.showEquip")
	for _, slot := range ce.slots() {
		print(" -> ", slot.id, " ", slot.item)
		if slot.item != nil {
			print(" name: ", slot.item.name)
		}
		print("\n")
	}
	println("#end")
}

/**** ItemSlot ****/

type ItemSlot struct {
	id   string
	name string
	item *Item // ref to item
}

func newItemSlot(id string, name string) (t ItemSlot) {
	t = ItemSlot{
		id:   id,
		name: name,
	}
	return
}

func (is *ItemSlot) isEmpty() bool {
	return is.item == nil
}

/*
func (is *ItemSlot) item() *Item {
	return is.item
}
*/
func (is *ItemSlot) setItem(it *Item) {
	is.item = it
}
