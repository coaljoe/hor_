package main

type LocationChunks map[string]*LocationChunk

// Main location structure.
type Location struct {
	id     string
	name   string
	chunks LocationChunks
	chunk  *LocationChunk // Active region
	items  ItemContainer
	// entry points
	// regions, parts, sublocations
}

func newLocation() *Location {
	loc := &Location{
		chunks: make(LocationChunks, 0),
		items:  newItemContainer()}
	return loc
}

func (l *Location) addChunk(lc *LocationChunk) {
	l.chunks[lc.name] = lc
}

func (l *Location) update(dt float64) {

}

////////////////
// LocationChunk

// A part of the location.
type LocationChunk struct {
	id      string
	name    string
	env     *Environment // Link to environment
	navgrid *Navgrid
	visited bool
	links   []*LocationChunks
	//children LocationRegions
}

func newLocationChunk() *LocationChunk {
	lc := &LocationChunk{}
	return lc
}
