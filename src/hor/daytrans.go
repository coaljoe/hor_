package main

// Day transition system

import (
	"fmt"
	"math"
	//"bitbucket.org/coaljoe/rx"
	"sync"

	"github.com/go-gl/gl/v2.1/gl"
	//"github.com/go-gl/glfw/v3.1/glfw"
)

var _ = gl.Init

type Daytrans struct {
	maxn   int
	n      int
	t      float64 // transition position
	tb     *TiledBackground
	active bool
}

func newDaytrans() *Daytrans {
	return &Daytrans{
		maxn:   8, //2, //8,
		n:      0, // 1?
		active: true,
	}
}

func (d *Daytrans) init(tb *TiledBackground, path string) {
	d.tb = tb

	var wg sync.WaitGroup

	// spawn layers
	for i := 0; i < d.maxn+1; i++ {
		nXX := d.nToStr(i)
		pathXX := path + "t" + nXX + "/"
		tb.addLayer(newLayer("bg_t"+nXX, tb.w, tb.h))
		tb.layers["bg_t"+nXX].load(pathXX, "bg", false)
		if tb.hasLayer("wall") {
			tb.addLayer(newLayer("wall_t"+nXX, tb.w, tb.h))
			tb.layers["wall_t"+nXX].load(pathXX, "wall", false)
			/*
				wg.Add(1)
				go func() {
					//defer wg.Done()
					//win := glfw.GetCurrentContext()
					cc := glfw.GetCurrentContext()
					fmt.Println("derp", cc)
					win := rx.Rxi().App.Win().GlfwWinSlave()
					win.MakeContextCurrent()

					if err := gl.Init(); err != nil {
						println("gl init filed")
						panic(err)
					}

					_ = win
					cc = glfw.GetCurrentContext()
					fmt.Println("win:", win)
					fmt.Println(cc)
					tb.layers["wall_t"+nXX].load(pathXX, "wall")
					//sleepsec(10)
					wg.Done()
				}()
			*/
		}
	}

	println("wainting wg...")
	wg.Wait()
	println("continue...")

	// load
}

func (d *Daytrans) getN() int {
	return d.n
}

func (d *Daytrans) getNextN() int {
	return d.n + 1
}

func (d *Daytrans) getNStr() string {
	return d.nToStr(d.getN())
}

func (d *Daytrans) getNextNStr() string {
	return d.nToStr(d.getNextN())
}

func (d *Daytrans) nToStr(n int) string {
	return fmt.Sprintf("%02d", n)
}

func (d *Daytrans) getT() float64 {
	// call update. fixme?
	d.update(game.timer.Dt())
	//fmt.Println(d.t)
	return d.t
}

func (d *Daytrans) update(dt float64) {
	if !d.active {
		return
	}

	inc := (0.0001 * dt)

	// switch n
	if (d.t + inc) >= 1.0 {
		d.n = (d.n + 1) % d.maxn
		_log.Inf("swith n; n =", d.n)
	}

	d.t = math.Mod(d.t+inc, 1.0)
}
