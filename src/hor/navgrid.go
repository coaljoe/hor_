package main

import (
	"bitbucket.org/coaljoe/rx"
	. "bitbucket.org/coaljoe/rx/math"
)

// Navigation grid.
type Navgrid struct {
	points []Vec3
	cells  [][]Cell
	w, h   int
	view   *NavgridView
}

func newNavgrid(w, h int) *Navgrid {
	n := &Navgrid{w: w, h: h}
	n.reset()
	return n
}

func (n *Navgrid) reset() {
	_log.Inf("[Navgrid] reset")
	cells := make([][]Cell, n.w)
	for i := range cells {
		cells[i] = make([]Cell, n.h)
	}
	n.cells = cells

	for x := 0; x < n.w; x++ {
		for y := 0; y < n.h; y++ {
			n.cells[x][y] = Cell{z: 0, open: true}
		}
	}
}

func (n *Navgrid) addFromMesh(path string) {
	sl := rx.NewSceneLoader()
	mn := sl.Load(path)[0]
	ps := mn.Mesh.GetMeshBuffer().GetVertices()
	n.points = append(n.points, ps...)
	//n.build()
}

func (n *Navgrid) isCellOpen(x, y int) bool {
	if n.cells[x][y].open {
		return true
	} else {
		return false
	}
}

/*
func (f *Field) isCellPassable(x, y int, r RealmType) bool {
	if r == RealmType_Ground && f.cells[x][y].z == 0 {
		return true
	} else if r == RealmType_Navy && f.cells[x][y].z < 0 {
		return true
	}
	return false
}
*/

// Cell height at x, y.
func (n *Navgrid) cellHeightAt(x, y int) float64 {
	return n.cells[x][y].z
}

func (n *Navgrid) spawn() {
	n.view = newNavgridView(n)
}

func (n *Navgrid) update(dt float64) {
	if n.view != nil {
		n.view.update(dt)
	}
}

///////
// Cell

// A Cell of the navgrid.
type Cell struct {
	z    float64 // Elevation
	open bool    // Is passable
}
