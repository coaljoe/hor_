package main

var maxObjId = 0

// component
type Obj struct {
	id int
}

func newObj() (t Obj) {
	t = Obj{
		id: maxObjId,
	}
	maxObjId++
	return
}
