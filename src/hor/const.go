package main

import (
	"github.com/go-gl/glfw/v3.1/glfw"
)

const (
	world_size = 1024 / 4
	cell_size  = world_size / 128 // warning, must be int?
)

var (
	fieldH, fieldW int
)

var vars struct {
	locateTargetsInterval float64
	scrollUpKey,
	scrollDownKey,
	scrollLeftKey,
	scrollRightKey glfw.Key
	scrollSpeed   float64
	scrollPadding int
}

func SetDefaultVars() {
	vars.locateTargetsInterval = 2.0
	vars.scrollUpKey = glfw.KeyUp
	vars.scrollDownKey = glfw.KeyDown
	vars.scrollLeftKey = glfw.KeyLeft
	vars.scrollRightKey = glfw.KeyRight
	vars.scrollSpeed = 5.0
	vars.scrollPadding = 40

	fieldH = 256
	fieldW = 256
}
