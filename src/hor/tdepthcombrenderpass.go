package main

import (
	"fmt"
	. "math"
	"bitbucket.org/coaljoe/rx"
	. "bitbucket.org/coaljoe/rx/math"

	"github.com/go-gl/gl/v2.1/gl"
)

var _ = Vec3Zero
var _ = fmt.Println

// **** TDepthCombRenderPass ****

type TDepthCombRenderPass struct {
	*rx.RenderPass
	//BgDepthTex *rx.Texture
	//BgTex *rx.Texture
	//dtex        *rx.Texture
	characterObj       *rx.Node
	//cutawayMask        *rx.Texture
	//cutawayMaskCard    *rx.Card
	//cutawayMaskFromPos Vec3
	//cutawayTexRt       *rx.RenderTarget
	//cutawayDepthRt     *rx.RenderTarget
	// fixme?
	combShader *rx.Shader
	//fsQuadShader   *rx.Shader
	sceneRt        *rx.RenderTarget
	depthRt        *rx.RenderTarget
	bgTexRt        *rx.RenderTarget
	bgDepthTexRt   *rx.RenderTarget
	//wallTexRt      *rx.RenderTarget
	//wallDepthTexRt *rx.RenderTarget
	tb             *TiledBackground
	tcam           *rx.Node
	//tcamNode *rx.CameraNode
	// daytrans
	daytransNextBgTexRt   *rx.RenderTarget
	daytransNextWallTexRt *rx.RenderTarget
	// overlaps
	//wallOverlap0TexRt      *rx.RenderTarget
	//wallOverlap0DepthTexRt *rx.RenderTarget
}

func NewTDepthCombRenderPass(tb *TiledBackground) *TDepthCombRenderPass {
	rp := &TDepthCombRenderPass{
		RenderPass: rx.NewRenderPass("TDepthRenderPass"),
		tb:         tb,
	}
	return rp
}

func (rp *TDepthCombRenderPass) Init(r *rx.Renderer) {
	// load resources
	/*
	rp.cutawayMask = rx.NewTextureOpt("res/textures/character_cutaway_mask4.png", rx.TextureOptions{
		MinFilter: rx.TextureFilterNearest,
		MagFilter: rx.TextureFilterNearest,
		Nomipmaps: true,
	})
	card := rx.NewCard()
	card.SetTexture(rp.cutawayMask)
	//card.SetDim(0.4, 0.4, 0.2, 0.4)
	card.SetDim(0.4, 0.4, r.GetPw()*float64(rp.cutawayMask.Width()),
		r.GetPh()*float64(rp.cutawayMask.Height()))
	card.SetVisible(false)
	card.Spawn()
	rp.cutawayMaskCard = card
	*/
	// fixme?
	//res_path := rx.ConfGetResPath()
	rp.combShader = rx.NewShader("shaders/camera_fsquad_depth_combine.vert",
		"shaders/camera_fsquad_depth_combine.frag")
	//rp.fsQuadShader = rx.NewShader("shaders/camera_fsquad.vert",
	//	"shaders/camera_fsquad.frag")
	width, height := r.Size()
	//rp.fboRt = rx.NewRenderTarget("fboRt", width, height, false)
	rp.sceneRt = rx.NewRenderTarget("sceneRt", width, height, false) // for dynamic objects
	rp.depthRt = rx.NewRenderTarget("depthRt", width, height, true)
	rp.bgTexRt = rx.NewRenderTarget("bgTexRt", width, height, false)
	rp.bgDepthTexRt = rx.NewRenderTarget("bgDepthTexRt", width, height, false)
	//rp.wallTexRt = rx.NewRenderTarget("wallTexRt", width, height, false)
	//rp.wallDepthTexRt = rx.NewRenderTarget("wallDepthTexRt", width, height, false)
	// cutaway
	//rp.cutawayTexRt = rx.NewRenderTarget("cutawayTexRt", width, height, false)
	//rp.cutawayDepthRt = rx.NewRenderTarget("cutawayDepthRt", width, height, true) // depth
	// daytrans
	rp.daytransNextBgTexRt = rx.NewRenderTarget("daytransNextBgTexRt", width, height, false)
	rp.daytransNextWallTexRt = rx.NewRenderTarget("daytransNextWallTexRt", width, height, false)
	// overlaps
	//rp.wallOverlap0TexRt = rx.NewRenderTarget("wallOverlap0TexRt", width, height, false)
	//rp.wallOverlap0DepthTexRt = rx.NewRenderTarget("wallOverlap0DepthTexRt", width, height, false)

	// tile cam
	rp.tcam = rx.NewCameraNode()
	//rp.tcam.Camera.SetupView(10, 1, 1.0, 100.0, true)
	rp.tcam.Camera.SetupView(10, 1, 10.0, 68.0, true) // 10-68 zdepth
}

func (rp *TDepthCombRenderPass) Render(r *rx.Renderer) {
	rp._update(r)

	cam := r.CurCam()

	//pp(rxi.Scene().renderables)
	rp.tb.setVisible(false)
	/*
	     	//r.RenderAll()
	     	//return
	       //gl.Enable(gl.TEXTURE_2D)
	     	rp.fboRt.Bind()
	     	gl.Clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT)
	     	r.RenderAll()
	     	rp.fboRt.Unbind()

	     	gl.ActiveTexture(gl.TEXTURE0)
	   	gl.BindTexture(gl.TEXTURE_2D, rp.fboRt.Tex())
	   	r.RenderFsQuad()
	   	gl.BindTexture(gl.TEXTURE_2D, 0)
	     	return
	*/

	// Render dynamic objects to fbo
	//
	rp.sceneRt.Bind()

	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	r.RenderAll()

	//rp.depthRt.Unbind()
	rp.sceneRt.Unbind()

	// Render bgTexRt
	//
	rp.bgTexRt.Bind()

	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	rp.tb.setVisible(true)
	//rp.tb.setLayer("bg")
	rp.tb.setLayer("bg_t" + _Daytrans.getNStr())

	// tiledbackground renderable
	//tbr := rxi.Scene().renderables[0]
	//tbr.Render()
	r.RenderRenderables()

	rp.bgTexRt.Unbind()
	rp.tb.setVisible(false)

	// Render bgDepthTexRt
	//
	rp.bgDepthTexRt.Bind()

	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	rp.tb.setVisible(true)
	rp.tb.setLayer("bg_depth")

	// tiledbackground renderable
	//tbr := rxi.Scene().renderables[0]
	//tbr.Render()
	r.RenderRenderables()

	rp.bgDepthTexRt.Unbind()
	rp.tb.setVisible(false)

	// --------------------

	var _=`
	// Render wallTexRt
	//
	rp.wallTexRt.Bind()

	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	rp.tb.setVisible(true)
	//rp.tb.setLayerIfVisible("wall")
	/*
		if rp.tb.layers["wall"].visible { // fixme
			//rp.tb.setLayerIfVisible("wall_t" + _Daytrans.getNStr())
			rp.tb.setLayer("wall_t" + _Daytrans.getNStr())
		}
	*/
	rp.tb.setLayer("wall_t" + _Daytrans.getNStr())

	// tiledbackground renderable
	//tbr := rxi.Scene().renderables[0]
	//tbr.Render()
	r.RenderRenderables()

	rp.bgTexRt.Unbind()
	rp.tb.setVisible(false)

	// Render wallDepthTexRt
	//
	rp.wallDepthTexRt.Bind()

	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	rp.tb.setVisible(true)
	rp.tb.setLayer("wall_depth")

	// tiledbackground renderable
	//tbr := rxi.Scene().renderables[0]
	//tbr.Render()
	r.RenderRenderables()

	rp.wallDepthTexRt.Unbind()
	rp.tb.setVisible(false)
	`

	// --------------------

	var _=`
	rp.cutawayTexRt.Bind()

	//r.ClearColor(1.0, 1.0, 1.0)
	r.Clear()

	rp.cutawayMaskCard.SetVisible(true)
	r.RenderRenderables()
	rp.cutawayMaskCard.SetVisible(false)

	rp.cutawayTexRt.Unbind()
	`

	// --------------------

	// Render daytransNextBgTexRt
	//
	rp.daytransNextBgTexRt.Bind()

	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	rp.tb.setVisible(true)
	rp.tb.setLayer("bg_t" + _Daytrans.getNextNStr())

	r.RenderRenderables()

	rp.daytransNextBgTexRt.Unbind()
	rp.tb.setVisible(false)

	// Render daytransNextWallTexRt
	//
	var _=`
	rp.daytransNextWallTexRt.Bind()

	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	rp.tb.setVisible(true)
	/*
		if rp.tb.layers["wall"].visible { // fixme
			rp.tb.setLayerIfVisible("wall_t" + _Daytrans.getNextNStr())
		}
	*/
	rp.tb.setLayerIfVisible("wall_t" + _Daytrans.getNextNStr())

	r.RenderRenderables()

	rp.daytransNextWallTexRt.Unbind()
	rp.tb.setVisible(false)
	`

	// --------------------

	var _=`

	// Render overlap0
	//
	rp.wallOverlap0TexRt.Bind()

	r.Clear()

	rp.tb.setVisible(true)

	//rp.tb.setLayer("wall_overlap0_t" + _Daytrans.getNStr())
	rp.tb.setLayer("wall_overlap0")

	r.RenderRenderables()

	rp.tb.setVisible(false)

	rp.wallOverlap0TexRt.Unbind()

	// Render overlap0 depth
	//
	rp.wallOverlap0DepthTexRt.Bind()

	r.Clear()

	rp.tb.setVisible(true)
	rp.tb.setLayer("wall_overlap0_depth")

	r.RenderRenderables()

	rp.tb.setVisible(false)

	rp.wallOverlap0DepthTexRt.Unbind()
	`

	// --------------------

	// Render scene to depthRt.
	//
	rp.depthRt.Bind()

	render_scene := func() {

		// clear tmp_rt texture
		gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

		/*
			r.RenderAll()

			rp.depthRt.Unbind()

		*/

		// render at center
		//r.SetViewport(rp.tb.shx + (r.Width()/2 - 512/2), rp.tb.shy + (r.Height()/2 - 512/2), 512, 512)
		/*
			r.SetViewport(r.Width()/2 - 512/2, r.Height()/2 - 512/2, 512, 512)
			rp.tcam.SetPos(cam.Pos())
			rp.tcam.SetRot(cam.Rot())
			r.SetCamera(rp.tcam)
		*/

		/*
			rp.tcam.SetPos(cam.Pos())
			rp.tcam.SetRot(cam.Rot())
			//p("old:", rp.tcam.Pos())
			//rp.tcam.MoveByVec(Vec3{10.0, 0.0, 0.0})
			//rp.tcam.MoveByVec(Vec3{10, 0.0, 0.0})
			//p("new:", rp.tcam.Pos())
			r.SetViewport(r.Width()/2 - 512/2, r.Height()/2 - 512/2, 512, 512)
			//r.SetViewport(100, 100, 512, 512)
			//r.SetViewport(512 + r.Width()/2 - 512/2, r.Height()/2 - 512/2, 512, 512)
			r.SetCamera(rp.tcam)
			r.RenderAll()

			rp.tcam.MoveByVec(Vec3{10, 0.0, 0.0})
			//rp.tcam.SetPos(Vec3{-4.611777305603027, -49.08281326293945, 13.023395538330078})
			r.SetViewport(512 + r.Width()/2 - 512/2, r.Height()/2 - 512/2, 512, 512)
			r.SetCamera(rp.tcam)
			r.RenderAll()
		*/

		var _ = `
		//cx := rp.tb.vpw/2
		//cy := rp.tb.vph/2
		cx := rp.tb.vpw/2 + (rp.tb.shx)
		cy := rp.tb.vph/2 + (rp.tb.shy)
		size := rp.tb.tileSize

		//rp.tcam.SetPos(cam.Pos())
		rp.tcam.SetRot(cam.Rot())
		rp.tcam.SetPos(Vec3{-14.17790, -46.17058, 12.93307}) // origin
		r.SetCamera(rp.tcam)
		//p("old:", rp.tcam.Pos())
		//rp.tcam.MoveByVec(Vec3{10.0, 0.0, 0.0})
		//rp.tcam.MoveByVec(Vec3{10, 0.0, 0.0})
		//p("new:", rp.tcam.Pos())

		// size in screen coords

		//r.SetViewport(cx - size/2, cy - size/2, size, size)
		//r.SetViewport(cx - size/2, cy - size/2, size, size)
		//r.SetViewport(cx - size/2, cy - size/2, size + rp.tb.shx, size + rp.tb.shy)
		/*
		ofy := 0
		if((cy - size/2) < 0) {
			ofy = cy - size/2
			p(ofy)
		}
		*/
		//r.SetViewport(cx - size/2, cy - size/2, size, size + ofy)
		r.SetViewport(cx - size/2, cy - size/2, size, size) 
		//r.SetViewport(cx - size/2, -100+cy - size/2, size, size)
		//r.SetViewport(100, 100, 512, 512)
		//r.SetViewport(512 + r.Width()/2 - 512/2, r.Height()/2 - 512/2, 512, 512)
		//r.SetCamera(rp.tcam)
		r.RenderAll()

		// misc
		r.SetViewport(0, 0, 100, 100)
		r.RenderAll()

		r.SetViewport(200, 200, 100, 100)
		r.RenderAll()


		// upper
		//rp.tcam.SetPos(Vec3{-12.59058, -6.61842, 12.93889})
		rp.tcam.MoveByVec(Vec3{-10, 10.0, 0.0})
		correct_height(rp.tcam)
		r.SetCamera(rp.tcam)
		//r.SetViewport((cx - size/2), 100 + (cy - size/2), size, size)
		//r.SetViewport(0, 200 + (cy - size/2), size, size)
		r.SetViewport(-size + (cx - size/2), size + (cy - size/2), size, size)
		r.RenderAll()
	`

		//var _=`
		//rp.tcam.SetPos(cam.Pos())
		rp.tcam.SetRot(cam.Rot())
		//rp.tcam.SetPos(rp.tb.cam.pos)
		//rp.tcam.SetRot(rp.tb.cam.rot)
		//tcam_origin := Vec3{-14.17790, -46.17058, 12.93307} // origin yxz
		//tcam_origin := Vec3{-14.177902221679688, -46.17058181762695, 12.933069229125977} // origin yxz precise
		tcam_origin := Vec3{-12.936243057250977, -46.53374099731445, 12.933069229125977} // origin yxz converted precise
		rp.tcam.SetPos(tcam_origin)
		r.SetCamera(rp.tcam)

		step := 10.0
		size := rp.tb.tileSize

		// skip x, y tiles from origin
		skipY := Floor(rp.tb.cam._moveAdj.Y() / 10)
		skipX := Floor(rp.tb.cam._moveAdj.X()/10) + (-skipY * 1)
		//skipY = 1
		var _ = skipX
		var _ = skipY
		/*
			// do z correction
			// note: must be always at x = 0 (+y*-1)
			if skipY > 0 || skipX > 0 {
				// save pos
				//pos := rp.tcam.Pos()
				// move y -x
				p(rp.tcam.Pos())
				rp.tcam.MoveByVec(Vec3{step*-skipX, step*skipY, 0})
				p(rp.tcam.Pos())
				correct_height(rp.tcam)
				p(rp.tcam.Pos())
				// restore pos
				rp.tcam.MoveByVec(Vec3{step*skipX, step*-skipY, 0})
				p(rp.tcam.Pos())
				//z := rp.tcam.Pos().Z
				//rp.tcam.SetPos(Vec3{pos.X, pos.Y(), z})

				p(skipX, skipY)
				pp("derp")
			}
		*/

		//rp.tcam.MoveByVec(Vec3{step*(skipX + -skipY), step*skipY, 0})
		rp.tcam.MoveByVec(Vec3{step * skipX, step * skipY, 0})
		//rp.tcam.MoveByVec(Vec3{0, step*skipY, 0})
		//rp.tcam.MoveByVec(Vec3{-10, 10, 0})
		//rp.tb.shy = -512
		//rp.tcam.MoveByVec(Vec3{0, 10, 0})
		//correct_height(rp.tcam)

		//p(skipX, skipY)sutt
		//rp.tcam.MoveByVec(Vec3{step*float64(skipX), step*float64(skipY), 0})

		tcam_render_origin := rp.tcam.Pos()
		r.SetCamera(rp.tcam)

		//cx := rp.tb.vpw/2 + (rp.tb.shx)
		//cy := rp.tb.vph/2 + (rp.tb.shy)
		//cy := rp.tb.vph/2 + (rp.tb.shy + 512)
		cx := rp.tb.vpw/2 + (rp.tb.shx + int(skipX)*size)
		cy := rp.tb.vph/2 + (rp.tb.shy + int(skipY)*size)
		sx, sy := rp.tb.vpSizeTiles()
		//sx = 2
		//sy = 2
		for y := 0; y <= sy; y++ {
			ysh := (y * size)
			for x := 0; x <= sx; x++ {
				//xsh := x*size
				xsh := (-y * size) + x*size
				//var _ = xsh
				//var _ = ysh
				//r.SetViewport(r.Width()/2 - size/2, r.Height()/2 - size/2, size, size)
				//r.SetViewport(xsh + (r.Width()/2 - size/2), r.Height()/2 - size/2, size, size)
				//r.SetViewport(xsh + (r.Width()/2 - size/2), ysh + (r.Height()/2 - size/2), size, size)
				/*
					r.SetViewport(rp.tb.shx + xsh + (r.Width()/2 - size/2),
								  rp.tb.shy + ysh + (r.Height()/2 - size/2), size, size)
				*/
				//r.SetViewport(xsh + ox, ysh + oy, size, size)

				//r.SetViewport(ox, oy + (y*size) - size/2, size, size)
				//r.SetViewport(ox, oy + (y*size) - size/2, size, size)
				r.SetViewport(xsh+(cx-size/2), ysh+(cy-size/2), size, size)
				r.SetCamera(rp.tcam)
				//r.SetViewport(ox, 100, size, size)
				//r.SetCamera(rp.tcam)
				r.RenderAll()
				//p("render x, y, pos: ", x, y, rp.tcam.Pos())

				// update x
				rp.tcam.MoveByVec(Vec3{step, 0, 0})
			}

			// update y

			// return
			//rp.tcam.SetPos(tcam_origin)
			rp.tcam.SetPos(tcam_render_origin)

			// shift y -x
			rp.tcam.MoveByVec(Vec3{-step * float64(y+1), step * float64(y+1), 0})
			//rp.tcam.MoveByVec(Vec3{0.0, step*float64(y+1), 0})

			correct_height(rp.tcam)
		}
		//pp(2)
		//`
	}

	render_scene()

	rp.depthRt.Unbind()

	// ------------------
	// render scene to cutawayDepthRt

	// Store visibility
	var _ = `
	var xv map[int]bool
	xv = make(map[int]bool)
	for _, n := range rx.Rxi().Scene().GetNodes() {
		p(n, n.Visible())
		n.SetVisible(false)
		p(n, n.Visible())
		if n.Visible() {
			xv[n.Id()] = true
			n.SetVisible(false)
		}
	}
	p("rerun...")
	for _, n := range rx.Rxi().Scene().GetNodes() {
		p(n, n.Visible())
		if n.Visible() {
			pp("fail")
		}
	}
	//pp(2)
	`

	box := rx.Rxi().Scene().GetNodeById(4) // Box Id
	box.SetVisible(true)

	//rp.cutawayDepthRt.Bind()

	render_scene()

	//rp.cutawayDepthRt.Unbind()
	box.SetVisible(false)

	/*
		// Restore visibility
		for _, n := range rx.Rxi().Scene().GetNodes() {
			if xv[n.Id()] != false {
				n.SetVisible(true)
			}
		}
	*/

	//-------------------

	// render scene
	//r.renderScene()
	//r.renderRenderables()
	//r.renderDebug()
	//r.RenderAll()

	r.SetViewport(0, 0, r.Width(), r.Height())
	r.SetCamera(cam)

	// Bind the RT's default fbo
	rp.FboRt().Bind()
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	// Render the cam.rt texture with fsquad shader.
	//
	rp.combShader.Bind()
	rp.combShader.PassUniform1i("fboTex", 0)
	rp.combShader.PassUniform1i("depthTex", 1)
	rp.combShader.PassUniform1i("bgTex", 2)
	rp.combShader.PassUniform1i("bgDepthTex", 3)
	//rp.combShader.PassUniform1i("wallTex", 4)
	//rp.combShader.PassUniform1i("wallDepthTex", 5)

	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, rp.sceneRt.Tex())
	gl.ActiveTexture(gl.TEXTURE1)
	gl.BindTexture(gl.TEXTURE_2D, rp.depthRt.Tex())
	//gl.BindTexture(gl.TEXTURE_2D, rp.dtex.Tex())
	gl.ActiveTexture(gl.TEXTURE2)
	//gl.BindTexture(gl.TEXTURE_2D, rp.BgTex.tex)
	gl.BindTexture(gl.TEXTURE_2D, rp.bgTexRt.Tex())
	gl.ActiveTexture(gl.TEXTURE3)
	//gl.BindTexture(gl.TEXTURE_2D, rp.BgDepthTex.tex)
	gl.BindTexture(gl.TEXTURE_2D, rp.bgDepthTexRt.Tex())
	//gl.ActiveTexture(gl.TEXTURE4)
	//gl.BindTexture(gl.TEXTURE_2D, rp.wallTexRt.Tex())
	//gl.ActiveTexture(gl.TEXTURE5)
	//gl.BindTexture(gl.TEXTURE_2D, rp.wallDepthTexRt.Tex())

	// cutaway
	//rp.combShader.PassUniform1i("cutawayTex", 6)
	//gl.ActiveTexture(gl.TEXTURE6)
	//gl.BindTexture(gl.TEXTURE_2D, rp.cutawayTexRt.Tex())

	// next daytrans textures
	rp.combShader.PassUniform1f("daytransT", _Daytrans.getT()) // fixme: no check for uniform
	// bg
	rp.combShader.PassUniform1i("daytransNextBgTex", 7)
	gl.ActiveTexture(gl.TEXTURE7)
	gl.BindTexture(gl.TEXTURE_2D, rp.daytransNextBgTexRt.Tex())
	/*
	// wall
	rp.combShader.PassUniform1i("daytransNextWallTex", 8)
	gl.ActiveTexture(gl.TEXTURE8)
	gl.BindTexture(gl.TEXTURE_2D, rp.daytransNextWallTexRt.Tex())

	// wallOverlap0
	// tex
	rp.combShader.PassUniform1i("wallOverlap0Tex", 9)
	gl.ActiveTexture(gl.TEXTURE9)
	gl.BindTexture(gl.TEXTURE_2D, rp.wallOverlap0TexRt.Tex())
	// depth
	rp.combShader.PassUniform1i("wallOverlap0DepthTex", 10)
	gl.ActiveTexture(gl.TEXTURE10)
	gl.BindTexture(gl.TEXTURE_2D, rp.wallOverlap0DepthTexRt.Tex())
	*/
	/*
		// character cutaway mask
		rp.combShader.PassUniform1i("cutawayMaskTex", 8)
		gl.ActiveTexture(gl.TEXTURE8)
		gl.BindTexture(gl.TEXTURE_2D, rp.cutawayMask.Tex())
		//rp.cutawayMask.Bind() // cannot use now, see texture.Bind
	*/

	/*
	// cutaway depth
	rp.combShader.PassUniform1i("cutawayDepthTex", 11)
	gl.ActiveTexture(gl.TEXTURE11)
	gl.BindTexture(gl.TEXTURE_2D, rp.cutawayDepthRt.Tex())
	*/

	var _ = `
	// mask's rect
	tw := (1.0 / float64(r.Width())) * float64(rp.cutawayMask.Width()) // screen space
	th := (1.0 / float64(r.Height())) * float64(rp.cutawayMask.Height())
	//pp(tw, th, rp.cutawayMask.Width(), rp.cutawayMask.Height())
	px, py := 0.4, 0.4
	rect := Vec4{px, py, px + tw, py + th}
	//pp(rect)
	rp.combShader.PassUniform4f_("cutawayMaskRect", rect)
	//pp(tw, th)
	`

	r.RenderFsQuad()

	rp.combShader.Unbind()
	gl.BindTexture(gl.TEXTURE_2D, 0)

	// unbind the fbo
	rp.FboRt().Unbind()

	// end

	/*
		// Pass1.
		// render the cam.rt texture with fsquad shader
		gl.Enable(gl.TEXTURE_2D)
		rp.cameraShader.Bind()

		gl.ActiveTexture(gl.TEXTURE0)
		//cam.rt.tex.Bind(gl.TEXTURE_2D)
		//gl.BindTexture(gl.TEXTURE_2D, cam.rt.tex)
		gl.BindTexture(gl.TEXTURE_2D, rp.tmpRt.tex)
		rp.cameraShader.PassUniform1i("fboTex", 0)
		rp.cameraShader.PassUniform1i("use_fxaa", btoi(rp.use_fxaa))

		r.RenderFsQuad()

		rp.cameraShader.Unbind()
		//cam.rt.tex.Unbind(gl.TEXTURE_2D)
		gl.BindTexture(gl.TEXTURE_2D, 0)

		rp.tmpRt.Unbind()
	*/
}

func (rp *TDepthCombRenderPass) SetCharacterObj(obj *rx.Node) {
	rp.characterObj = obj
}

func (rp *TDepthCombRenderPass) _update(r *rx.Renderer) {
	var _=`
	if rp.characterObj != nil {
		// calculate characterObj's window coords

		scrW := float64(r.Width())
		scrH := float64(r.Height())

		// project
		ob := rp.characterObj
		//obj := ob.Pos() // object coords
		obHeight := 1.8
		//obWidth := 0.4
		// approx. height center of the character
		obj := ob.Pos().Add(Vec3{0, 0, obHeight / 2.0})
		//obj := ob.Pos()
		cam := _Scene.camera.cam
		//modelviewM := ob.Mat().Mul(cam.GetViewMatrix())
		modelviewM := cam.Camera.GetViewMatrix()
		projM := cam.Camera.GetProjectionMatrix()
		// opengl window coords
		win := Project(obj, modelviewM, projM, 0, 0, int(scrW), int(scrH))
		//fmt.Println("win: ", win)

		if win.X() > 0 && win.Y() > 0 && win.X() < scrW && win.Y() < scrH {
			card := rp.cutawayMaskCard
			// set card's center at win coord
			card.SetDim((win.X()/scrW)-card.Width()/2, (win.Y()/scrH)-card.Height()/2, card.Width(), card.Height())
		}
	}
	`
}

// From blender version.
func correct_height(ob *rx.Node) { // CameraNode
	// http://www.cleavebooks.co.uk/scol/calrtri.htm
	A := -0.0 //None
	//B := 75.0
	B := 74.99997818140304 // precise
	C := 90.0

	//a = 12.93307
	//a = ob.location.z
	a := ob.Pos().Z()
	b := -0.0 //None
	c := -0.0 //None

	// find c
	// solve asa
	// https://www.mathsisfun.com/algebra/trig-solving-sas-triangles.html

	// find angle A
	A = 180.0 - B - C
	//print("A:", A)

	// find side c
	// a/sinA = c/sinC

	c = (a * Sin(Radians(C))) / Sin(Radians(A))
	//print("c:", c)

	// find b
	// b/sinB = c/sinC
	b = (a * Sin(Radians(B))) / Sin(Radians(A))
	//print("b:", b)

	// building small triangle

	A2 := A
	B2 := B
	C2 := C

	// target heigth
	//a2 = 5.0
	//a2 := 12.93307
	a2 := 12.933069229125977 // precise
	b2 := -0.0               //None
	c2 := -0.0               //None

	c2 = (a2 * Sin(Radians(C2))) / Sin(Radians(A2))
	//print("c2:", c2)

	r := c - c2
	//print("r:", r)

	//# move ob

	/*
		tmp := ob.Rot()
		_r := tmp
		_r[1] = 0
		//_r[2] = 0
		//_r[0] = 0
		p(tmp, _r)
		ob.SetRot(_r)
	*/
	v := Vec3{0.0, 0.0, -r}
	ob.MoveByVec(v)
	/*
		// restore camera y rotation
		ob.SetRot(tmp)
	*/
	// unused
	var _ = b
	var _ = B2
	var _ = b2
}
