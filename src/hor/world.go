package main

import (
	"time"
)

// WorldInfo?
type World struct {
	env     Environment
	daytime time.Time
	test    bool
}

func newWorld() (w World) {
	w = World{
		env:  newEnvironment(),
		test: true,
	}
	_log.Dbg("newWorld")
	return
}

func (w *World) timeOfDay() string {
	h := w.daytime.Hour()
	pname := ""
	switch {
	case h >= 8 && h < 10:
		pname = "dawn"
	case h >= 10 && h < 15:
		pname = "noon"
	case h >= 15 && h < 20:
		pname = "dusk"
	case (h >= 20 && h < 24) || h < 8:
		pname = "night"
	default:
		pp("unknown time of day;", h)
	}
	return pname
}

func (w *World) setDaytime(fmt, s string) {
	if v, err := time.Parse(fmt, s); err != nil {
		_log.Err("can't set daytime")
		panic(err)
	} else {
		w.daytime = v
	}
	_log.Dbg(w.daytime)
}

func (w *World) daytimeStr() string {
	return w.daytime.Format("15:04 06/01/02")
}

func (w *World) setDefaultWorld() {
	// time
	w.setDaytime(time.RFC3339, "2001-01-01T12:00:00Z")
	// environment
	env := w.env
	env.temperature = -10
	env.windSpeed = 1
	_log.Dbg(w.daytime)
	println("HERP")
}

func (w *World) update(dt float64) {

}
