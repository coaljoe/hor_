package main

import (
	"bytes"
	"fmt"
	"log"
	"reflect"
	"time"
)

/** Log **/

//var logger
/*
func init() {
  println("util.log init")
}
*/

func Log1(msg string) {
	var buf bytes.Buffer
	logger := log.New(&buf, "log: ", log.Lshortfile)
	logger.Print(msg)

	fmt.Print(&buf)
}

func cknil(c interface{}) {
	if c == nil || reflect.ValueOf(c).IsNil() {
		panic("Check nil failed.")
	}
}

func cknilBool(c interface{}) bool {
	if c == nil || reflect.ValueOf(c).IsNil() {
		return true
	}
	return false
}

func p(args ...interface{}) {
	fmt.Println(args)
}

func pp(args ...interface{}) {
	fmt.Println(args)
	panic("pp")
}

func sleepsec(t float64) {
	// sleep float seconds
	time.Sleep(time.Duration(t*1000) * time.Millisecond)
}
