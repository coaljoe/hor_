package main

import (
	ps "bitbucket.org/coaljoe/lib/pubsub"
)

const (
	ev_obj_spawn ps.EventType = iota
)

func Pub(eventType ps.EventType, data interface{}) {
	ps.Publish(eventType, data)
}

func Sub(eventType ps.EventType, fn ps.Callback) {
	ps.Subscribe(eventType, fn)
}
