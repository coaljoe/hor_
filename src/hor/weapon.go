package main

// Компонент итема отвечающий за ф-ции оружия.
// component
type Weapon struct {
	item           *Item
	rounds         int
	maxRounds      int
	attackRange    float64 // m
	attackInterval float64 // s
	reloadTime     float64 // s
	useCounter     int
}

func newWeapon(it *Item) (w Weapon) {
	w = Weapon{
		item:           it,
		attackRange:    10,
		attackInterval: 1,
		reloadTime:     1,
		useCounter:     0,
	}
	return
}

func (w *Weapon) fire() {
	_log.Dbg("weapon.fire")
	if w.rounds == 0 {
		if !w.reload() {
			_log.Dbg("no bullets")
			return
		}
	}

	// fire
	w.rounds -= 1
	w.useCounter += 1
}

func (w *Weapon) reload() bool {
	_log.Dbg("weapon.reload")
	// todo: use ammo
	w.rounds = w.maxRounds
	return true
}

func mini(a, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

func maxi(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}
