package main

var (
	_ItemSys *ItemSys
	_World   *World
)

// gamestate
type State struct {
	itemsys ItemSys
	world   World
}

func newState() (s *State) {
	s = &State{
		itemsys: newItemSys(),
		world:   newWorld(),
	}
	// global vars
	_ItemSys = &s.itemsys
	_World = &s.world
	p("eq  test", *_World == s.world)
	p("eq  test", _World == &s.world)
	return
}

func (s *State) setDefaultState() {
	s.world.setDefaultWorld()
}

func (s *State) update(dt float64) {
	//itemsys.update(dt)
}
