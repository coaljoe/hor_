package main

/**** Item container ****/

// Контейнер предметов.
// Содержит предметы как ссылки на итемы.
type ItemContainer struct {
	name,
	contName string
	items map[*Item]bool
	maxWeight,
	maxBulk float64
}

func newItemContainer() (t ItemContainer) {
	t = ItemContainer{
		items: make(map[*Item]bool),
	}
	return t
}

func (ic *ItemContainer) addItem(it *Item) bool {
	ic.items[it] = true
	it.setContainer(ic)
	return true
}

func (ic *ItemContainer) removeItem(it *Item) bool {
	if !ic.hasItem(it) {
		panic("no such item")
		//return false
	}
	delete(ic.items, it)
	it.unsetContainer()
	return true
}

func (ic *ItemContainer) hasItem(it *Item) bool {
	//return cknilBool(ic.items[it.id]) == false
	_, ok := ic.items[it]
	return ok
	//return ic.items[it] != nil
}

func (ic *ItemContainer) swapItem(it *Item, oth *ItemContainer) bool {
	_log.Dbg("ItemContainer swapItem;", it, oth)
	if it.inContainer() {
		ic.removeItem(it)
	}
	oth.addItem(it)
	return true
}

func (ic *ItemContainer) showItems() {
	println("#ItemContainer.showItems")
	for it := range ic.items {
		println(" >", it, it.name)
	}
	println("#")
}

/**** Inventory ****/

type Inventory struct {
	cont ItemContainer
}

func newInventory() (t Inventory) {
	t = Inventory{
		cont: newItemContainer(),
	}
	return
}

func (i *Inventory) putItem(it *Item) bool {
	i.cont.addItem(it)
	return true
}

func (i *Inventory) takeItem(it *Item) bool {
	i.cont.removeItem(it)
	return true
}
