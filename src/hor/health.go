package main

// component
type Health struct {
	health int
}

func newHealth() (t Health) {
	t = Health{
		health: 100,
	}
	return
}

func (h *Health) showHealth() {
	println("Health: ", h.health)
}

func (h *Health) setHealth(v int) {
	println("setHealth: ", v)
	h.health = v
}

func (h *Health) takeHealth(amt int) {
	println("takeHealth: ", amt)
	h.setHealth(h.health - amt)
}

func (h *Health) isAlive() bool {
	return h.health > 0
}
