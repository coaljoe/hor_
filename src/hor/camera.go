package main

import (
	"fmt"
	ps "bitbucket.org/coaljoe/lib/pubsub"
	. "math"
	"bitbucket.org/coaljoe/rx"
	. "bitbucket.org/coaljoe/rx/math"

	"github.com/go-gl/glfw/v3.1/glfw"
)

type CameraState int

const (
	CameraIdleState CameraState = iota
	CameraScrollingState
)

type Camera struct {
	cam    *rx.Node
	mx, my int
	radius,
	rotSpeed,
	rotStep,
	tilt,
	tiltMin,
	tiltMax,
	tiltStep,
	zoom,
	zoomSpeed,
	zoomStep,
	zoomMin,
	zoomMax,
	defaultZoom,
	velocity,
	maxVelocity float64
	state    CameraState
	rotAdj   float64
	_moveAdj Vec3 // fixme
}

func newCamera() (t *Camera) {
	t = &Camera{
		state: CameraIdleState,
	}
	t.setDefaultCamera()
	Sub(rx.Ev_mouse_move, t.onMouseMove)
	Sub(rx.Ev_key_press, t.onKeyPress)
	Sub(rx.Ev_key_release, t.onKeyRelease)
	return
}

func (c *Camera) setDefaultCamera() {
	c.radius = Sqrt(Pow(10, 2) + Pow(10, 2)) // diagonal
	c.rotSpeed = 60
	c.rotStep = 15 //360/16
	c.tilt = 35.264
	c.tiltMin = 20
	c.tiltMax = 60
	c.tiltStep = 10
	c.zoom = 10
	c.zoomSpeed = 1
	c.zoomStep = 1
	c.zoomMin = 1
	c.zoomMax = 10
	c.defaultZoom = 3
	//c.maxVelocity = 10
	c.maxVelocity = vars.scrollSpeed

	// Add camera.
	//
	rxi := rx.Rxi()
	//app.rxi.camera.transform(10, 10, 10,  -35.264, 45, 0)
	camn := rxi.Camera
	camn.Camera.SetZoom(c.zoom)
	//cam.SetZoom(10)
	//cam.Pos = rx.NewVector3(10, 20, 10)
	//cam.Rot = rx.NewVector3(35.264, 45, 0)
	//cam.Pos = rx.NewVector3(0, 50, 0)
	//cam.Rot = rx.NewVector3(90, 0, 0)
	//cam.SetPos(Vec3{-5, 5, -5})
	//cam.SetPos(Vec3{5, 5, 5})
	//cam.SetPos(Vec3{30, 30, 30})
	//cam.SetPos(c._pos)
	//cam.Rot = rx.NewVector3(90+35.264, 45, 0)
	//cam.Rot = rx.NewVector3(35.264, 45, 0)

	//cam.SetTarget(c.trg)
	println(camn.Pos().String())
	println(camn.Rot().String())
	c.cam = camn

	c.setPos(Vec3{30, 30, 30})
	c.setRot(Vec3{0, 0, 0})
	//c.trg = Vec3{0, 0, 0}
}

func (c *Camera) onMouseMove(ev *ps.Event) {
	//p := ev.Data.(struct{ x, y int })
	p := ev.Data.(rx.EvMouseMoveData)
	println("cam.onmousemove", p.X, p.Y)
	c.mx = p.X
	c.my = p.Y
}

func (c *Camera) onKeyPress(ev *ps.Event) {
	key := ev.Data.(glfw.Key)
	println("cam.onkeypress", key)
	//dt := game().app.GetDt() // fixme
	/*
		switch k {
		case vars.scrollUpKey, vars.scrollDownKey,
				vars.scrollLeftKey, vars.scrollRightKey:
			//c.pos.Y() += vars.scrollSpeed * dt
			c.velocity = c.maxVelocity
		}
	*/
	var adj, adjamt float64
	adjamt = c.rotSpeed
	if key == glfw.KeyZ {
		adj = +adjamt
	} else if key == glfw.KeyX {
		adj = -adjamt
	} else {
		return
	}
	println("adj", adj)
	c.rotAdj = adj
}

func (c *Camera) onKeyRelease(ev *ps.Event) {
	key := ev.Data.(glfw.Key)
	println("cam.onkeyrelease", key)
	switch key {
	case glfw.KeyZ, glfw.KeyX:
		c.rotAdj = 0
	default:
		return
	}
}

func (c *Camera) _update(dt float64) {
	// update zoom
	if c.zoom != c.cam.Camera.Zoom() {
		c.cam.Camera.SetZoom(c.zoom)
	}

	// fixme
	//c.cam.SetPos(c._pos)
	//c.cam.SetRot(c._rot)
	//c.cam.SetTarget(c.trg)
}

func (c *Camera) _pan(x, y float64) {
	//c.cam.MoveByVec(Vec3{0, 0, .1})
	v := 0.0
	if y > 0 {
		v = .1
	} else {
		v = -.1
	}
	/*
		tmp := c.cam.Rot()

		// set camera y rotation to zero
		r := tmp
		r[1] = 0
		//r[2] = 0
		//r[0] = 0
		//p(tmp, r, x, y)
		c.cam.SetRot(r)
	*/
	//c.cam.MoveByVec(Vec3{0, v, 0})
	c.cam.MoveByVec(Vec3{0, 0, v})
	/*
		// restore camera y rotation
		c.cam.SetRot(tmp)
	*/
	fmt.Println("Pan", x, y)
	fmt.Printf("cam pan\n -pos: %s\n -rot: %s\n -_moveAdj: %s\n", c.pos(), c.rot(), c._moveAdj)
}

func (c *Camera) pan(x, y float64) {
	//c.Translate(x, y, 0)

	/*
		tmp := c.cam.Rot()

		// set camera y rotation to zero
		r := tmp
		r[1] = 0
		//r[2] = 0
		//r[0] = 0
		p(tmp, r, x, y)
		c.cam.SetRot(r)

		c.cam.MoveByVec(Vec3{x, y, 0})

		// restore camera y rotation
		c.cam.SetRot(tmp)
	*/
	c.cam.MoveByVec(Vec3{x, y, 0})

	//c.cam.Translate(Vec3{x, y, 0})

	//c._pos = c.cam.Pos() // update c.pos

	// FIXME: not checked, seems buggy
	//correct_height(c.cam)

	// fixme: update _moveAdj
	c._moveAdj = c._moveAdj.Add(Vec3{x, y, 0})
	//c.build()
	fmt.Printf("cam pan\n -pos: %s\n -rot: %s\n -_moveAdj: %s\n", c.pos(), c.rot(), c._moveAdj)
}

func (c *Camera) build() {
	//c._pos = c.cam.Pos() // update c.pos
	//c.cam.SetPos(c._pos)
	//c.cam.SetRot(c._rot)
}

func (c *Camera) pos() Vec3 {
	return c.cam.Pos()
}

func (c *Camera) setPos(v Vec3) {
	//c._pos = v
	//c.build()
	c.cam.SetPos(v)
}

func (c *Camera) rot() Vec3 {
	return c.cam.Rot()
}

func (c *Camera) setRot(v Vec3) {
	//c._rot = v
	//c.build()
	c.cam.SetRot(v)
}

// fixme: deprecated? use Pan(?)
func (c *Camera) translate(x, y, z float64) {
	c.cam.Translate(Vec3{x, y, z})
	//c.cam.MoveByVec(Vec3{x, y, 0})
	// fixme: update _moveAdj
	c._moveAdj = c._moveAdj.Add(Vec3{x, y, z})
	c.build()
}

func (c *Camera) update(dt float64) {
	kb := _InputSys.Keyboard
	m := _InputSys.Mouse
	sw, sh := game.app.Win().Size()
	var px, pz float64
	var vv, vh Vec3
	ary := Radians(c.cam.Rot().Y())
	if kb.IsKeyPressed(vars.scrollUpKey) ||
		m.Y < vars.scrollPadding {
		px = -Sin(ary)
		pz = -Cos(ary)
		vv = Vec3{0, 1, 0}
		c.velocity = c.maxVelocity
	} else if kb.IsKeyPressed(vars.scrollDownKey) ||
		m.Y >= sh-vars.scrollPadding {
		px = Sin(ary)
		pz = Cos(ary)
		vv = Vec3{0, -1, 0}
		c.velocity = c.maxVelocity
	}
	if kb.IsKeyPressed(vars.scrollLeftKey) ||
		m.X < vars.scrollPadding {
		px = -Sin(ary + Radians(90.0))
		pz = -Cos(ary + Radians(90.0))
		vh = Vec3{-1, 0, 0}
		c.velocity = c.maxVelocity
	} else if kb.IsKeyPressed(vars.scrollRightKey) ||
		m.X >= sw-vars.scrollPadding {
		px = Sin(ary + Radians(90.0))
		pz = Cos(ary + Radians(90.0))
		vh = Vec3{1, 0, 0}
		c.velocity = c.maxVelocity
	}
	/*
		moveAdj := Vec3{px * curVel, 0, pz * curVel}
		c.pos = c.pos.Add(moveAdj)
		c.trg = c.trg.Add(moveAdj)
	*/

	// update zoom
	if c.zoom != c.cam.Camera.Zoom() {
		c.cam.Camera.SetZoom(c.zoom)
	}

	//return

	// update pos
	if c.velocity > 0 {
		_, _ = px, pz
		curVel := c.velocity * dt
		moveAdj := vv.MulScalar(curVel).Add(vh.MulScalar(curVel))
		p("moveAdj", moveAdj)
		//c.pos = c.pos.Add(moveAdj)
		//c.trg = c.trg.Add(moveAdj)

		// per-pixel panning adjacements
		uSizePx := 512.0 / 10.0 // unit size pixels
		pxSizeU := 10.0 / 512.0 // pixel size units
		//moveAdj.X = moveAdj.X * pxSizeU
		//moveAdj.Y() = moveAdj.Y() * pxSizeU
		//c.Pan(moveAdj.X * pxSizeU, moveAdj.Y() * pxSizeU)
		//c.Pan(Floor(moveAdj.X + .5) * pxSizeU, Ceil(moveAdj.Y()) * pxSizeU)
		//c.Pan(1 * pxSizeU, Ceil(moveAdj.Y()) * pxSizeU)
		c.pan(
			Floor(moveAdj.X()*uSizePx+.5)*pxSizeU,
			Floor(moveAdj.Y()*uSizePx+.5)*pxSizeU)
		//p("modeAdj", moveAdj.X(), moveAdj.X()*uSizePx)
		/*
			c.cam.MoveByVec(moveAdj)
			c.pos = c.cam.Pos() // update c.pos

			c._moveAdj = c._moveAdj.Add(moveAdj)
			//p(moveAdj, c._moveAdj, c.pos)
		*/
		// reset velocity
		c.velocity = 0
	}
	//c.cam.SetPos(c._pos)
	//c.cam.SetRot(c._rot)
	//c.cam.SetTarget(c.trg)

	/*
	   	// update rot
	   	adjrot := false
	   	if adjrot {
	   		// adj rot
	   		new_rot := Mod(c.rot + c.rotAdj * dt, 360)
	     		c.rot = new_rot

	     		// fix: not working
	     		r := Abs(c.pos.X - c.trg.X)
	     		//p(r)
	     		aa := Radians(c.rot)
	     		new_pos := c.pos
	     		//new_pos.X = r * Sin(aa) + c.trg.X
	     		//new_pos.Z = r * Cos(aa) + c.trg.Z
	     		new_pos.X = r * Sin(aa) + c.pos.X
	     		new_pos.Z = r * Cos(aa) + c.pos.Z
	     		//c.pos = new_pos
	     		c.cam.SetPos(new_pos)
	     	}
	*/

	//p("new rot", c.rot, c.rotAdj)
}
