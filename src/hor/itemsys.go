package main

type ItemSys struct {
	items map[*Item]bool
}

func newItemSys() (is ItemSys) {
	is = ItemSys{
		items: make(map[*Item]bool),
	}
	return
}

func (is *ItemSys) addItem(it *Item) {
	is.items[it] = true
}

func (is *ItemSys) removeItem(it *Item) {
	delete(is.items, it)
}
