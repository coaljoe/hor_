package main

type ItemCondition int

const (
	icUnknown ItemCondition = iota
	icGood
	icBad
	icBroken
)

type ItemI interface {
}

// Предмет.
// Может содержать множество ссылок на себя, передается всегда как референс.
// Один предмет может храниться только в одном контейнере.
// entity
type Item struct {
	Obj
	appliable *AppliableItem
	equipable *EquipableItem
	weapon    *Weapon
	name      string
	weight,
	bulk float64
	condition  ItemCondition
	durability float64
	description,
	longDescription string
	// item's holder/host
	cont *ItemContainer
	// inventory
	invIcon string
}

func newItem() (i *Item) {
	i = &Item{
		Obj:        newObj(),
		condition:  icUnknown,
		durability: 1.0,
		invIcon:    "noimage",
	}
	_ItemSys.addItem(i)
	return
}

func (it *Item) isAppliable() bool {
	return it.appliable != nil
}

func (it *Item) isEquipable() bool {
	return it.equipable != nil
}

func (it *Item) isWeapon() bool {
	return it.weapon != nil
}

func (it *Item) inContainer() bool {
	return it.cont != nil
}

func (it *Item) setContainer(cont *ItemContainer) {
	it.cont = cont
}

func (it *Item) unsetContainer() {
	it.cont = nil
}

func (it *Item) Destroy() {
	_ItemSys.removeItem(it)
}

func (it *Item) String() string {
	return "<Item name: " + it.name + ">"
}

/**** Appliable Item ****/

// component
type AppliableItem struct {
	item         *Item
	applyMessage string
}

type AppliableItemI interface {
	apply()
}

func newAppliableItem(item *Item) (t AppliableItem) {
	t = AppliableItem{
		item: item,
	}
	return
}

func (ai *AppliableItem) apply() {
	println("item applied; msg: ", ai.applyMessage)
	println("item name:", ai.item.name)
}

/**** Equipable Item ****/

// component
type EquipableItem struct {
	item       *Item
	wearer     *Character
	insulation int
	itemSlotId string
}

func newEquipableItem(item *Item) (t EquipableItem) {
	t = EquipableItem{
		item: item,
	}
	return
}
