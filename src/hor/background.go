package main

import (
	"bitbucket.org/coaljoe/rx"
)

type Background struct {
	tex  *rx.Texture
	view *BackgroundView
	z    float64
}

func NewBackground() (t *Background) {
	t = &Background{}
	return
}

func (b *Background) SetZ(z float64) {
	b.z = z
}

func (b *Background) Load(path string) {
	b.tex = rx.NewTexture(path)
	//b.tex.Load()
}

func (b *Background) Spawn() {
	b.view = NewBackgroundView(b)
	b.view.Spawn()
}

func (b *Background) Update(dt float64) {

	// update view
	if b.view != nil {
		b.view.Update(dt)
	}
}
