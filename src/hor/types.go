package main

type UpdateableI interface {
	update(dt float64)
}
