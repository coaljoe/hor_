package main

import "bitbucket.org/coaljoe/rx"

type TiledBackgroundView struct {
	*rx.Node
	m *TiledBackground
}

func newTiledBackgroundView(m *TiledBackground) *TiledBackgroundView {
	v := &TiledBackgroundView{
		Node: rx.NewNode(rx.NodeType_Custom),
		m:    m,
	}
	return v
}

func (v *TiledBackgroundView) Spawn() {
	rx.Rxi().Scene().AddRenderable(v)
}

func (v *TiledBackgroundView) Destroy() {
	rx.Rxi().Scene().RemoveRenderable(v)
}

func (v *TiledBackgroundView) Render(r *rx.Renderer) {
	m := v.m

	if !m.visible {
		return
	}

	tex := m.texs[0][0]
	size := m.tileSize

	// screen center
	//cx := m.vpw/2 - size/2
	//cy := m.vph/2 - size/2
	cx := m.vpw/2 + m.shx
	cy := m.vph/2 + m.shy

	// rendering size in tiles
	//sx := int(m.vpw / size+1)
	//sy := int(m.vph / size+1)
	sx, sy := m.vpSizeTiles()

	// lower tile
	lty := int(-m.shy / size)
	ltx := int(-m.shx / size)
	for y := lty; y < lty+sy+1; y++ {
		for x := ltx; x < ltx+sx+1; x++ {

			// skip overflow tiles
			if x < 0 || x > m.w-1 ||
				y < 0 || y > m.h-1 {
				continue
			}

			tex = m.texs[x][y]

			// row shift
			rsh := -(y * size)

			if tex != nil {
				tex.Bind()
			}

			r.RenderQuadPx(rsh+cx+(x*size)-size/2, cy+(y*size)-size/2, size, size, 0)
			_ = cx
			_ = cy
			_ = rsh

			if tex != nil {
				tex.Unbind()
			}
		}
	}

	//r.RenderQuadPx(m.shx + cx, m.shy + cy, size, size, 0)
	//r.RenderQuadPx(m.shx + cx + size, m.shy + cy, size, size, 0)
	//r.RenderQuad(0, 0, float64(size), float64(size), 0)
	//r.RenderQuad(0, 0, 512, 512, 0)
	//r.RenderQuadPx(512, 0, 128, 128, 0)
	//r.RenderQuadPx(0, 100, 128, 128, 0)

}

func (v *TiledBackgroundView) update(dt float64) {
}
