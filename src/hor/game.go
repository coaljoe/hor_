package main

import (
	//. "fmt"
	//. "math"
	//"ltest"
	"bitbucket.org/coaljoe/rx"
)

var (
	game      *Game = nil
	_InputSys *rx.InputSys
	_Scene    *Scene
	_Daytrans *Daytrans
	_Location *Location
)

func initNewGame(app *rx.App) bool {
	_ = newGame(app)
	return true
}

type Game struct {
	app      *rx.App
	timer    *Timer
	inputsys *rx.InputSys
	state    *State
	scene    *Scene
	daytrans *Daytrans
	location *Location
}

func newGame(app *rx.App) *Game {
	if game != nil {
		return game
	}

	println("game.new")
	SetDefaultVars()
	g := &Game{
		app:   app,
		timer: NewTimer(true),
	}
	g.inputsys = rx.NewInputSys(g.app)
	g.state = newState()
	g.scene = NewScene(g)
	g.daytrans = newDaytrans()
	g.location = newLocation()

	// set default game
	g.setDefaultGame()

	// set access vars
	_InputSys = g.inputsys
	_Scene = g.scene
	_Daytrans = g.daytrans

	game = g
	return game
}

func (g *Game) setDefaultGame() {
	g.scene.setDefaultScene()
	g.state.setDefaultState()
}

/*
func game() *Game {
	return game
}
*/

func (g *Game) update(dt float64) {
	dt = dt * g.timer.Speed()
	g.timer.Update(dt)

	g.scene.Update(dt)
	g.location.update(dt)
}
