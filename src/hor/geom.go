package main

import (
	. "math"
)

func Round_(x float64, prec int) float64 {
	var rounder float64
	pow := Pow(10, float64(prec))
	intermed := x * pow
	_, frac := Modf(intermed)
	if frac >= 0.5 {
		rounder = Ceil(intermed)
	} else {
		rounder = Floor(intermed)
	}

	return rounder / pow
}
