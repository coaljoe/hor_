package main

import (
	"bytes"
	"fmt"
	"log"
	"os"

	js "github.com/bitly/go-simplejson"
	//"path"
	"regexp"
	//. "strconv"
)

/**** Item factory ****/

// Make item from data
func makeItem(itemName string) *Item {
	itemsdir := "res/items"
	fp := fmt.Sprintf("%s/%s/%s.json", itemsdir, itemName, itemName)
	js := getJsonFromFile(fp)

	// Load Item
	def := js.Get("Item")

	it := newItem()

	if d, ok := def.CheckGet("name"); ok {
		it.name = d.MustString()
	} else {
		fail()
	}
	if d, ok := def.CheckGet("weight"); ok {
		it.weight = d.MustFloat64()
	} else {
		fail()
	}
	if d, ok := def.CheckGet("bulk"); ok {
		it.bulk = d.MustFloat64()
	} else {
		fail()
	}
	if d, ok := def.CheckGet("description"); ok {
		it.description = d.MustString()
	} else {
		fail()
	}
	if d, ok := def.CheckGet("longDescription"); ok {
		it.longDescription = d.MustString()
	} else {
		//optional
	}
	if d, ok := def.CheckGet("invIcon"); ok {
		it.invIcon = d.MustString()
	} else {
		fail()
	}

	// Load AppliableItem
	if def, ok := js.CheckGet("AppliableItem"); ok {
		ai := newAppliableItem(it)
		it.appliable = &ai

		if d, ok := def.CheckGet("applyMessage"); ok {
			ai.applyMessage = d.MustString()
		} else {
			fail()
		}
	}

	// Load EquipableItem
	if def, ok := js.CheckGet("EquipableItem"); ok {
		ei := newEquipableItem(it)
		it.equipable = &ei

		if d, ok := def.CheckGet("itemSlotId"); ok {
			ei.itemSlotId = d.MustString()
		} else {
			fail()
		}
		if d, ok := def.CheckGet("insulation"); ok {
			ei.insulation = d.MustInt()
		} else {
			// optional
		}
	}

	// Load Weapon
	if def, ok := js.CheckGet("Weapon"); ok {
		w := newWeapon(it)
		it.weapon = &w

		if d, ok := def.CheckGet("maxRounds"); ok {
			w.maxRounds = d.MustInt()
		} else {
			fail()
		}
		if d, ok := def.CheckGet("attackRange"); ok {
			w.attackRange = d.MustFloat64()
		} else {
			fail()
		}
		if d, ok := def.CheckGet("attackInterval"); ok {
			w.attackInterval = d.MustFloat64()
		} else {
			fail()
		}
		if d, ok := def.CheckGet("reloadTime"); ok {
			w.reloadTime = d.MustFloat64()
		} else {
			fail()
		}
	}

	return it
}

/**** location factory ****/

// make location from data
func makeLocation(locName string) *Location {
	locsdir := "res/locations"
	fp := fmt.Sprintf("%s/%s/%s.json", locsdir, locName, locName)
	js := getJsonFromFile(fp)

	// load location
	def := js.Get("item")
	l := newLocation()

	if d, ok := def.CheckGet("name"); ok {
		l.name = d.MustString()
	} else {
		fail()
	}

	// load location regions
	if lrBlk, ok := js.CheckGet("LocationRegions"); ok {
		// load location region
		for i := range lrBlk.MustArray() {
			lrBlk := js.Get("LocationRegions").GetIndex(i)
			if d, ok := lrBlk.CheckGet("name"); ok {
				var _ = d
			} else {
				fail()
			}
		}
	}

	return l
}

/**** misc ****/

func fail() { panic("json read fail") }

func getJsonFromFile(fp string) *js.Json {
	r, err := os.Open(fp)
	if err != nil {
		wd, _ := os.Getwd()
		println("wd:", wd)
		println("cant read file: " + fp)
		log.Fatal(err)
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(r)
	s := buf.String()

	reg, err := regexp.Compile("//.*?\n")
	if err != nil {
		log.Fatal(err)
	}
	safe := reg.ReplaceAllString(s, "\n")
	//Println(safe)

	//d, err := js.NewFromReader(r)
	js, err := js.NewJson([]byte(safe))
	if err != nil {
		fmt.Println(safe)
		println("cant read json from file: " + fp)
		log.Fatal(err)
	}

	return js
}
