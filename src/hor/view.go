package main

import (
	"lib/ecs"
	"bitbucket.org/coaljoe/rx"
)

type ViewI interface {
	//Render()
	Spawn()
	Update(dt float64)
	Destroy()
	//GetMeshNodeByName(name string) *rx.Node
	GetEntity() *ecs.Entity
}

type ViewCmpI interface {
	//Render()
	Spawn()
	Update(dt float64)
	//NodeName() string
	//GetMeshNode() *rx.Node
	Destroy()
}

/** View **/

type View struct {
	*ecs.Entity
}

func NewView() *View {
	ent := ecs.NewEntity()
	return &View{
		Entity: ent,
	}
}

func (v *View) GetEntity() *ecs.Entity { return v.Entity }

/*
func (v *View) Render() {
  for _, c := range v.Components() {
    cc := c.(ViewCmpI)
    cc.Render()
  }
}
*/

func (v *View) Spawn() {
	for _, c := range v.Components() {
		cc := c.(ViewCmpI)
		cc.Spawn()
	}
}

func (v *View) Destroy() {
	for _, c := range v.Components() {
		cc := c.(ViewCmpI)
		cc.Destroy()
	}
}

func (v *View) Update(dt float64) {
	for _, c := range v.Components() {
		cc := c.(ViewCmpI)
		cc.Update(dt)
	}
}

/** ViewCmp **/

type ViewCmp struct {
	nodeName string
	ddir     string
	node     *rx.Node
	loaded,
	spawned bool
}

func NewViewCmp() *ViewCmp {
	t := &ViewCmp{}
	return t
}

//func (vc *ViewCmp) NodeName() string { return vc.nodeName}
//func (vc *ViewCmp) GetMeshNode() *rx.Node { return vc.node }
func (vc *ViewCmp) Destroy()          {}
func (vc *ViewCmp) Update(df float64) {}
