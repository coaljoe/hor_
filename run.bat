@echo off
setlocal
set PATH=%PATH%;lib\dll
set GOPATH=%CD%\..\go;%CD%
@rem set GOMAXPROCS=0
set GODEBUG=efence=1

set LOGLEVEL=INFO

@rem set mingw env
rem call "C:\Program Files\mingw-w64\x86_64-5.3.0-posix-seh-rt_v4-rev0\mingw-w64.bat"
rem set PATH=C:\Program Files\mingw-w64\x86_64-5.3.0-posix-seh-rt_v4-rev0\mingw64\bin;%PATH%
set PATH=C:\users\k\k\progs\mingw-w64\x86_64-6.2.0-posix-seh-rt_v5-rev1\mingw64\bin;%PATH%

@rem always clean
@rem go clean -i rx
go install -v rx rx/math
@rem del hor.exe

@rem go run -v src\main.go
@rem go build -i hor
go install -v hor
bin\hor.exe %*

:end
endlocal
