uniform vec3 lightPos;

uniform mat4 shadowMatrix;
varying vec4 shadowCoord;

varying vec3 N;
varying vec3 L;
varying vec3 V;
varying vec3 H;

#ifdef _TEXTURE_MAP
//vec2 vertexUV;
//vec2 UV;
#endif
