uniform sampler2D fboTex;
uniform sampler2D depthTex;
uniform sampler2D bgTex;
uniform sampler2D bgDepthTex;
uniform sampler2D wallTex;
uniform sampler2D wallDepthTex;
uniform vec2 ScreenPos;

float LinearizeDepth(float zoverw)
{
    float n = 1.0;
    float f = 40.0;
	
    return(2.0 * n) / (f + n - zoverw * (f - n));	
}

float LinearizeDepth2(float zoverw)
{
    float n = 1.0;
    float f = 100.0;
	
    //return (zoverw - 1.0) * (f - n);
    //return n + zoverw*(f - n);
    return(2.0 * n) / (n + zoverw * (f - n));	
}

/*
float LinearizeDepth3(float z_b)
{
    float zNear = 1.0;
    float zFar = 100.0;
	
    float z_n = 2.0 * z_b - 1.0;
    float z_e = 2.0 * zNear * zFar / (zFar + zNear - z_n * (zFar - zNear));

	return z_e;
}
*/
float LinearizeDepth4(float z)
{
    float n = 1.0;
    float f = 100.0;
    /*float f = 100.0;*/

	//return n * (z + 1.0) / (f + n - z * (f - n));
	return (2.0 * n) / (f + n - z * (f - n));
	//return n * (z + 1.0) / (f + n - z * (f - n));
    /*float z_n = 2.0 * z - 1.0;*/
	//return (z_n - n) / f;
	/*return (z_n - n) * f;*/
    /*float z_e = 2.0 * n * f / (f + n - z_n * (f - n));*/
	/*return z_e;*/
}

void main()
{
    vec4 r;

	// y-inverted tex coords
	//vec2 tc_inv = vec2(gl_TexCoord[0].s, 1.0 - gl_TexCoord[0].t);

    // not using inverted tc, flip textures for fbo instead.
    // fixme: move to vs for speed?
	vec2 tc = gl_TexCoord[0].st;

    // FboTex
    vec4 color = texture2D(fboTex, tc);

    // DepthTex
    float depth = texture2D(depthTex, tc).r;
    //depth = LinearizeDepth(depth)*77;
    //depth = LinearizeDepth(depth)*6;

    // BgTex
    vec4 bgColor = texture2D(bgTex, tc);

    // BgDepthTex
    float bgDepth = texture2D(bgDepthTex, tc).r;

    // BgTex
    vec4 wallColor = texture2D(wallTex, tc);

    // BgDepthTex
    float wallDepth = texture2D(wallDepthTex, tc).r;

    /*gl_FragColor = vec4(depth, depth, depth, 1.0);*/
    /*gl_FragColor.rgb = vec3(color);*/
    /*gl_FragC = vec4(bgcolor);*/

#if 0
    if (gl_TexCoord[0].x < 0.5) {
        //r = vec4(bgDepth, bgDepth, bgDepth, 1.0);
        r = color;
    } else {
		/*depth = LinearizeDepth4(depth)*31;*/
		//depth = LinearizeDepth4(depth)*30;
		//depth = LinearizeDepth4(depth)*12;
		/*depth = LinearizeDepth4(depth);*/
		/*depth = LinearizeDepth4(depth);*/
                depth = depth;
                /*depth = 0.1 + (depth * 2.3);*/
		/*depth = depth * (1.0/2.2);*/
                /*depth = depth * 6.6;*/
		/*depth = (depth + 1.0) * 0.5;*/ // -1,1 -> 0,1
		//depth = LinearizeDepth(depth)*7.0;
		/*depth = LinearizeDepth(depth)*5.5;*/
        /*depth = LinearizeDepth(depth)*14.8;*/
        /*depth = LinearizeDepth(depth)*17.5;*/
		/*depth = LinearizeDepth2(depth)*2;*/
        r = vec4(depth, depth, depth, 1.0);
        //gl_FragColor.rgb = vec3(0,1,0);
    }
#endif

    //if (depth < bgDepth) {
    if (depth < min(wallDepth, bgDepth)) {
    //if (wallDepth < 0.1 || (depth > wallDepth)) {
	//if (wallDepth > 0.1) {
        r = color;
    } else {
        //r = bgColor;
        //r = wallColor;
		//if (bgDepth >= wallDepth + 0.01) {
		if (bgDepth >= wallDepth) {
			r = wallColor;
		} else {
			r = bgColor;
		}
        //r = bgColor + color.r * 0.1;
        //r = bgColor + (((color.r + color.g + color.b)/3) * 0.05);
        //r = bgColor + 0.05;
        /*
        vec4 v = vec4(0, 0, 0, 0);
        if(max(color,v) == color) {
            float sw = 960;
            float sh = 540;
            if(int(gl_TexCoord[0].y * sh) % 2 == 0 && 
               int(gl_TexCoord[0].x * sw) % 2 != 0) {
                float avg = ((bgColor.r + bgColor.g + bgColor.b)/3.0);
                r = vec4(avg, avg, avg, 1.0);
                //r = color;
             }
         }
         */
    }

    /*
    if (gl_TexCoord[0].x < 0.5) {
        r = bgColor;
    } else {
        r = color;
    }
    */

    // gamma corrected
    // #r = r * (1.0/2.2); // wrong
    /*
    float g = 1.0/2.2;
    r = pow(r, vec4(g, g, g, 1.0));
    */

    gl_FragColor = vec4(r.r, r.g, r.b, 1.0);
}

/* vim:set ft=glsl: */
