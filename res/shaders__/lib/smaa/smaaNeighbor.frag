// Neighborhood fragment shader

uniform sampler2D texture;
uniform sampler2D blend_tex;
vec2 texcoord;
vec4 offset[2];
vec4 dummy2;

void main()
{
  gl_FragColor = SMAANeighborhoodBlendingPS(texcoord, offset, texture, blend_tex);
  //gl_FragColor = texture2D(blend_tex, texcoord);//gl_TexCoord[0].xy);
  //gl_FragColor.a = 1.0;
}

