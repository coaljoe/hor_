#define SMAA_GLSL_3 1
#define SMAA_PRESET_LOW 1
#define SMAA_INCLUDE_PS 0
#define SMAA_INCLUDE_VS 1
#define SMAA_RT_METRICS float4(1.0 / 800.0, 1.0 / 600.0, 800.0, 600.0)

#include "smaa.h"

varying vec2 ScreenPos;

void main()
{
    ScreenPos = gl_MultiTexCoord0.st;
    gl_TexCoord[0] = gl_MultiTexCoord0; // screen texcoord

    gl_Position = ftransform();
}