#define FXAA_PC 1
//#define FXAA_GLSL_130 1

// 120 not supported on notebook(?)
#define FXAA_GLSL_120 1
//#extension GL_EXT_gpu_shader4 : enable

//#define FXAA_QUALITY_PRESET 12
//#define FXAA_QUALITY_PRESET 39

//#define FXAA_QUALITY__PRESET 39
// fastest
//#define FXAA_QUALITY__PRESET 10
//#define FXAA_QUALITY__PRESET 13
//#define FXAA_QUALITY__PRESET 13
//#define FXAA_QUALITY__PRESET 12
 // too noizy?
//#define FXAA_QUALITY__PRESET 15
// looks good
#define FXAA_QUALITY__PRESET 23

// note: doesn't work with 0?
// the output must be RGBL
//#define FXAA_GREEN_AS_LUMA 0
#define FXAA_GREEN_AS_LUMA 1
//#define FXAA_FAST_PIXEL_OFFSET 0
//#define FXAA_GATHER4_ALPHA 0

//#include "Fxaa3_11.h"
#include "fxaa3_11_fp_glsl120.glsl"
//derp
