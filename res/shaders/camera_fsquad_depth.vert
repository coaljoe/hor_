varying vec2 ScreenPos;
varying float depth;
varying vec4 position_;

void main()
{
    ScreenPos = gl_MultiTexCoord0.st;
    gl_TexCoord[0] = gl_MultiTexCoord0; // screen texcoord

    /*
    float near = 0.0;
    float far = 1.0;
    //float near = 0.1;
    //float far = 1000;

    vec4 viewPos = gl_ModelViewMatrix * gl_Vertex; // this will transform the vertex into eyespace
    depth = (-viewPos.z-near)/(far-near); // will map near..far to 0..1
    */

    /*
    vec4 viewPos = gl_ModelViewMatrix * gl_Vertex; // this will transform the vertex into eyespace
    depth = -viewPos.z; // minus because in OpenGL we are looking in the negative z-direction
    */

/*
    position_ = gl_ModelViewProjectionMatrix * gl_Vertex;
*/

    gl_Position = ftransform();
}

/* vim:set ft=glsl: */
