uniform sampler2D fboTex; // sceneRt
uniform sampler2D depthTex; // depthRt
uniform sampler2D bgTex;
uniform sampler2D bgDepthTex;
uniform sampler2D wallTex;
uniform sampler2D wallDepthTex;
uniform sampler2D cutawayTex; // cutawayRt
uniform sampler2D cutawayDepthTex; // cutawayDepthRt
//uniform sampler2D cutawayMaskTex;
//uniform vec4 cutawayMaskTexSize; // = vec2(0.2, 0.2);
//uniform vec4 cutawayMaskRect; // = vec4(0.4, 0.4, 0.6, 0.6); // screen-space
//uniform vec2 characterPos2D = vec2(0.5, 0.5) // screen-space character pos
// daytrans
uniform float daytransT;// = 0.5;
uniform sampler2D daytransNextBgTex;
uniform sampler2D daytransNextWallTex;
// overlap
uniform sampler2D wallOverlap0Tex;
uniform sampler2D wallOverlap0DepthTex;

// debug color;
const vec3 c_debug = vec3(0.1, 0.1, 1.0);
const vec4 c_debug4 = vec4(0.1, 0.1, 1.0, 1.0);

// return 1 if v inside the box, return 0 otherwise
float InRect(vec2 v, vec2 bottomLeft, vec2 topRight) {
    vec2 s = step(bottomLeft, v) - step(topRight, v);
    return s.x * s.y;
}

float LinearizeDepth(float zoverw)
{
    float n = 1.0;
    float f = 40.0;
	
    return(2.0 * n) / (f + n - zoverw * (f - n));	
}

float LinearizeDepth2(float zoverw)
{
    float n = 1.0;
    float f = 100.0;
	
    //return (zoverw - 1.0) * (f - n);
    //return n + zoverw*(f - n);
    return(2.0 * n) / (n + zoverw * (f - n));	
}

/*
float LinearizeDepth3(float z_b)
{
    float zNear = 1.0;
    float zFar = 100.0;
	
    float z_n = 2.0 * z_b - 1.0;
    float z_e = 2.0 * zNear * zFar / (zFar + zNear - z_n * (zFar - zNear));

	return z_e;
}
*/
float LinearizeDepth4(float z)
{
    float n = 1.0;
    float f = 100.0;
    /*float f = 100.0;*/

	//return n * (z + 1.0) / (f + n - z * (f - n));
	return (2.0 * n) / (f + n - z * (f - n));
	//return n * (z + 1.0) / (f + n - z * (f - n));
    /*float z_n = 2.0 * z - 1.0;*/
	//return (z_n - n) / f;
	/*return (z_n - n) * f;*/
    /*float z_e = 2.0 * n * f / (f + n - z_n * (f - n));*/
	/*return z_e;*/
}

void main()
{
    vec4 r;

	// y-inverted tex coords
	//vec2 tc_inv = vec2(gl_TexCoord[0].s, 1.0 - gl_TexCoord[0].t);

    // not using inverted tc, flip textures for fbo instead.
    // fixme: move to vs for speed?
	vec2 tc = gl_TexCoord[0].st;

    // FboTex
    vec4 color = texture2D(fboTex, tc);

    // DepthTex
    float depth = texture2D(depthTex, tc).r;
    //float depth = texture2D(cutawayDepthTex, tc).r;
    //depth = LinearizeDepth(depth)*77;
    //depth = LinearizeDepth(depth)*6;

    // BgTex
    //vec4 bgColor = texture2D(bgTex, tc);
	vec4 bgColor = mix(texture2D(bgTex, tc),
					   texture2D(daytransNextBgTex, tc), daytransT);

    // BgDepthTex
    float bgDepth = texture2D(bgDepthTex, tc).r;

    // WallTex
    //vec4 wallColor = texture2D(wallTex, tc);
	vec4 wallColor = mix(texture2D(wallTex, tc),
					     texture2D(daytransNextWallTex, tc), daytransT);

    // WallDepthTex
    float wallDepth = texture2D(wallDepthTex, tc).r;

	
	vec4 cutawayColor = texture2D(cutawayTex, tc);
	
	vec4 wallOverlap0Color = texture2D(wallOverlap0Tex, tc);
	float wallOverlap0Depth = texture2D(wallOverlap0DepthTex, tc).r;

    /*gl_FragColor = vec4(depth, depth, depth, 1.0);*/
    /*gl_FragColor.rgb = vec3(color);*/
    /*gl_FragC = vec4(bgcolor);*/

#if 0
    if (gl_TexCoord[0].x < 0.5) {
        //r = vec4(bgDepth, bgDepth, bgDepth, 1.0);
        r = color;
    } else {
		/*depth = LinearizeDepth4(depth)*31;*/
		//depth = LinearizeDepth4(depth)*30;
		//depth = LinearizeDepth4(depth)*12;
		/*depth = LinearizeDepth4(depth);*/
		/*depth = LinearizeDepth4(depth);*/
                depth = depth;
                /*depth = 0.1 + (depth * 2.3);*/
		/*depth = depth * (1.0/2.2);*/
                /*depth = depth * 6.6;*/
		/*depth = (depth + 1.0) * 0.5;*/ // -1,1 -> 0,1
		//depth = LinearizeDepth(depth)*7.0;
		/*depth = LinearizeDepth(depth)*5.5;*/
        /*depth = LinearizeDepth(depth)*14.8;*/
        /*depth = LinearizeDepth(depth)*17.5;*/
		/*depth = LinearizeDepth2(depth)*2;*/
        r = vec4(depth, depth, depth, 1.0);
        //gl_FragColor.rgb = vec3(0,1,0);
    }
#endif
	
	// fill with bg color by default
	r = bgColor;
	//r = color;
	
    if (depth < bgDepth) {
    //if (depth < min(wallDepth, bgDepth)) {
	//{
    //if (wallDepth < 0.1 || (depth > wallDepth)) {
	//if (wallDepth > 0.1) {
	    /// Is a dynamic object.
        r = color;
    } else {
        r = bgColor;
        //r = wallColor;
		//if (bgDepth >= wallDepth + 0.01) {
		//if (bgDepth >= wallDepth)

		/*
			float maskVal = cutawayColor.r;
			if (depth > bgDepth) {
				/// A dynamic object behind the wall.
				//r = c_debug4;
				//r = mix(wallColor, color, maskVal);
			}
		*/
		float maskVal = texture2D(cutawayTex, tc).r;
        float cutawayDepth = texture2D(cutawayDepthTex, tc).r;
		
		if (bgDepth > wallDepth)
		{
			/// Is a wall.
			r = wallColor; // dup
			//r = c_debug4;
			//r = mix(wallColor, bgColor, maskVal);
			//r = mix(wallColor, bgColor, 0.5); // whole wall trans
			if (cutawayDepth < wallDepth) {
    			//r = mix(wallColor, bgColor, maskVal);
    			//r = c_debug4; // draw debug affect-zone
    			r = wallColor;
			}
			else {
    			//r = mix(wallColor, bgColor, maskVal);
    			//r = mix(wallColor, bgColor, cutawayDepth);
    			//r = mix(wallColor, bgColor, 1.0-cutawayDepth); // transp cutaway
    			//r = mix(wallColor, bgColor, 1.0-(cutawayDepth/2)); // transp test (nice for debug)
				if (cutawayDepth < 1.0) {
	    			r = mix(wallColor, bgColor, maskVal);
				}
			}
		} else {
			/// Just mixing wall with the bg using mask.
			//r = mix(wallColor, bgColor, 1.0-maskVal);
			//r = mix(wallColor, (color*0.9 + bgColor*0.1), maskVal); // nice effect(?)
		}
		
	}

#if 0

    //if (depth < bgDepth) {
    if (depth < min(wallDepth, bgDepth)) {
	//{
    //if (wallDepth < 0.1 || (depth > wallDepth)) {
	//if (wallDepth > 0.1) {
	    /// Is a dynamic object.
        r = color;
    } else {
        //r = bgColor;
        //r = wallColor;
		//if (bgDepth >= wallDepth + 0.01) {
		//if (bgDepth >= wallDepth)
		if (bgDepth > wallDepth)
		{
			/// Is a wall.
			r = wallColor; // dup
			//r = c_debug4;
			//r = cutawayColor; // debug

			//float maskVal = texture2D(cutawayTex, tc).r;
			float maskVal = cutawayColor.r;

			/*
			// hard edges
			if (maskVal != 1.0) {
				return;
			}
			*/

			//r = bgColor;
			//if (depth < bgDepth) {
			//if ((depth * maskVal) < (bgDepth * maskVal)) {
			/* src one munus alpha custom mix code
			* c = (1.0 - t1.a) * t0 + t1.a * t1;
			* from: distrustsimplicity.net/articles/texture-blending-in-glsl/
			*/
			/*
			if (depth < bgDepth) {
				//r = color;
				//r = mix(bgColor, color, maskVal);
				//r = mix(wallColor, color, maskVal);
				//r = mix(wallColor, color, maskVal * 2);
				r = c_debug4;
			}
			*/
			{
				//r = bgColor;
				//r = bgColor * maskVal; // black walls
				//r = bgColor * 1.0;
				//r = mix(color, bgColor, maskVal);
				//r = mix(wallColor, bgColor, maskVal);
			}

			if (depth < bgDepth) {
				/// A dynamic object behind the wall.
				//r = c_debug4;
				r = mix(wallColor, color, maskVal);
			}
			else {
				/// Just mixing wall with the bg using mask.
				//r = mix(wallColor, bgColor, maskVal);
				//r = mix(wallColor, wallOverlap0Color, maskVal);
				
				vec4 bc = bgColor;
				if (wallDepth < wallOverlap0Depth) {
					bc = wallOverlap0Color;
				}
				r = mix(wallColor, bc, maskVal);
				
				//r = mix(wallColor, (color*0.5 + bgColor*0.5), maskVal); // nice effect(?)
				//r = mix(wallColor, bgColor*0.5, maskVal);
				//r = mix(wallColor, (color*0.9 + bgColor*0.1), maskVal); // nice effect(?)
				/*
				   if (maskVal != 0.0) {
						   if (depth < 0.9 && depth > bgDepth) {
								   r = c_debug4;
						   }

				   }
				 */
			}


			/*
			if (maskVal != 0.0) {

			}
			*/

			// fill with debug color
			//r = c_debug4;
		}
		//r = bgColor;
        //r = bgColor + color.r * 0.1;
        //r = bgColor + (((color.r + color.g + color.b)/3) * 0.05);
        //r = bgColor + 0.05;
        /*
        vec4 v = vec4(0, 0, 0, 0);
        if(max(color,v) == color) {
            float sw = 960;
            float sh = 540;
            if(int(gl_TexCoord[0].y * sh) % 2 == 0 && 
               int(gl_TexCoord[0].x * sw) % 2 != 0) {
                float avg = ((bgColor.r + bgColor.g + bgColor.b)/3.0);
                r = vec4(avg, avg, avg, 1.0);
                //r = color;
             }
         }
         */
    }
#endif
	/*
	// bg select
		if (depth < 0.9 && depth > bgDepth) {
			r = c_debug4;
		}
	*/
	
	
	/*
		// transparent in-front objects
		if (depth < 0.9 && depth > bgDepth) {
			//r = mix(bgColor, color*0.5, 0.2);
			r = mix(bgColor, vec4(0.2, 0.1, 0.5, 1.0)+(color*0.2), 0.25); // glass effect
		}
	*/
	
	// transparent in-front objects
	if (depth < 0.9 && depth > bgDepth) {
		//r = mix(bgColor, color*0.5, 0.2);
		//r = mix(bgColor, vec4(0.2, 0.1, 0.5, 1.0)+(color*0.2), 0.25); // glass effect
        float avg = ((bgColor.r + bgColor.g + bgColor.b)/3.0);
		if ( avg < 0.4)  {
    		r = mix(bgColor, vec4(color.g, color.g, color.g, 1.0)*1.5, 0.8);
		}
	}
	

	// texture test
	//r = wallColor;
	//r = cutawayColor;
	//r = vec4(1.0) * bgDepth;
	//r = vec4(1.0) * wallDepth;

    /*
    if (gl_TexCoord[0].x < 0.5) {
        r = bgColor;
    } else {
        r = color;
    }
    */

    // gamma corrected
    // #r = r * (1.0/2.2); // wrong
    /*
    float g = 1.0/2.2;
    r = pow(r, vec4(g, g, g, 1.0));
    */

    gl_FragColor = vec4(r.r, r.g, r.b, 1.0);
}

/* vim:set ft=glsl: */
