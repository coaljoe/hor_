#include "uniforms.frag"
#include "lighting.frag"


void main() {
    vec3 color;

    color = ambient + calcLambertLight(L, N, mat.diffuse, light.intensity);

#ifdef _TEXTURE_MAP
    color *= texture2D(texMap, gl_TexCoord[0].st).rgb;
#endif

    color *= calcShadow();

    gl_FragColor.rgb = color;
}

/* vim:set ft=glsl: */
