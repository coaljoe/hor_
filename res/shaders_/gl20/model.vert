#include "uniforms.vert"

void main(void)
{
    // vertex position
    vec3 p = vec3(gl_ModelViewMatrix * gl_Vertex);

    N = normalize(gl_NormalMatrix*gl_Normal);
    //N = normalize(gl_Normal);
    V = p;
    //L = vec3(gl_ModelViewMatrix*(vec4(lightPos,1)-gl_Vertex));

    // point light
    //L = lightPos - p;

    // directional light
    L = normalize(lightPos);

    //L = gl_LightSource[0].position.xyz;
    //L = vec3(1, 1, 1); // world space
    //L = lightPos;
    //H = normalize(L + normalize(V));

    H = normalize(normalize(L) + normalize(V));

    // calculate shadow
    shadowCoord = shadowMatrix * gl_ModelViewMatrix * gl_Vertex;

#ifdef _NORMAL_MAP
    vec3 n = normalize (gl_NormalMatrix * gl_Normal);
    vec3 t = normalize (gl_NormalMatrix * gl_MultiTexCoord7.xyz);
    //vec3 t = normalize (gl_NormalMatrix * tangent);
    //vec3 t = normalize (gl_NormalMatrix * (gl_Color.rgb - 0.5));
    vec3 b = cross (n,t) * gl_MultiTexCoord7.w;

    mat3 tbn = mat3(t,b,n);
    
    /*
    L = normalize(L) * tbn;
    L = normalize(L);

    H = normalize(L + normalize(V));
    H = H * tbn;
    */
    L = L * tbn;
    V = -p * tbn;
#endif

    gl_TexCoord[0] = gl_MultiTexCoord0; // texture
    gl_TexCoord[1] = gl_MultiTexCoord1; // lightmap
    gl_TexCoord[2] = gl_MultiTexCoord0; // normalmap

    //UV = vertexUV;
    //gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;

    //gl_Position = gl_ProjectionMatrix * vec4(p,1);
    gl_Position = ftransform();
}

/* vim:set ft=glsl: */
